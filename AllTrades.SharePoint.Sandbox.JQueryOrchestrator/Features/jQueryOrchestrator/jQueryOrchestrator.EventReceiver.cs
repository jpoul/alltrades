using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Security;

namespace AllTrades.SharePoint.Sandbox.JQueryOrchestrator.Features.jQueryOrchestrator
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("c4c939b4-86fa-480d-a47d-238c2c0b5ed5")]
    public class jQueryOrchestratorEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties) {
            using (SPSite site = (SPSite)properties.Feature.Parent)
            {
                SPList list = site.RootWeb.GetList(site.RootWeb.ServerRelativeUrl + "/js");
                if (list != null && !list.ContentTypes[0].Id.ToString().Contains("73E5D20EC8D94EA8B59D85459E51F5D2"))
                {
                    list.ContentTypes.Delete(list.ContentTypes[0].Id);
                    list.Update();
                }
                // Get the obsolete content type.
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
