﻿//Function that Dynamically Loads the Javascript
function loadScript(scriptSrc, sitecollectionurl) {

    //Gets a reference to the Head section, where the Script tage will be inserted
    var headSection = document.getElementsByTagName('head')[0];

    //Creates a new Script tag
    var script = document.createElement('script');
    script.type = 'text/javascript';

    //Sets the source for the script tag to our external library
    script.src = sitecollectionurl + 'js/' + scriptSrc;

    //Adds the section to the header
    headSection.appendChild(script);
}

function InitializeJQueryScripts(sitecollectionurl) {

    $(document).ready(function () {
        
        $().SPServices({
            operation: 'GetListItems',
            webURL: sitecollectionurl,
            async: false,
            listName: 'jQuery Scripts Library',
            CAMLViewFields: '<ViewFields><FieldRef Name=\'LinkFilenameNoMenu\' /></ViewFields>',
            CAMLQuery: '<Query><Where><And><Neq><FieldRef Name=\'FromSolution\' /><Value Type=\'Integer\'>1</Value></Neq><Neq><FieldRef Name=\'DisableScript\' /><Value Type=\'Integer\'>1</Value></Neq></And></Where><OrderBy><FieldRef Name=\'Sequence\' Ascending=\'TRUE\' /></OrderBy><ViewAttributes Scope=\'Recursive\'/></Query>',
            completefunc: function (xData, Status) {
                //alert(xData.responseText);
                $(xData.responseXML).SPFilterNode('z:row').each(function () {
                    var pageRegex = $(this).attr('ows_ApplyTo');

                    if (window.location.pathname.match(pageRegex)) {
                        var jscriptfile = $(this).attr('ows_LinkFilenameNoMenu');
                        loadScript(jscriptfile, sitecollectionurl);
                        
                    }
                });
            }
        });
    });
}
