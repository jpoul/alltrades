﻿using System;
using System.IO;
using AllTrades.Utils;
using ICSharpCode.SharpZipLib.Zip;

namespace AllTrades.Files
{
    public class ZipFileWrapper : IDisposable
    {
        private bool _disposed;

        public ZipFileWrapper(Stream outStream)
        {
            ZipStream = new ZipOutputStream(outStream);
            //ZipStream.SetLevel(9); //best compression
            //ZipStream.UseZip64 = UseZip64.On;
            Factory = new ZipEntryFactory(DateTime.Now);
        }

        protected ZipOutputStream ZipStream { get; private set; }
        private ZipEntryFactory Factory { get; set; }

        #region IDisposable Members

        public void Dispose()
        {
            Close();
        }

        #endregion

        public void Add(string fileName, Stream fileStream)
        {
            //create a new zip entry            
            ZipEntry entry = Factory.MakeFileEntry(fileName);
            entry.DateTime = DateTime.Now;
            ZipStream.PutNextEntry(entry);
            byte[] bytes = StreamUtils.ReadFully(fileStream, 0);
            ZipStream.Write(bytes, 0, bytes.Length);
        }

        public void AddDirectory(string directoryName)
        {
            ZipEntry entry = Factory.MakeDirectoryEntry(directoryName);
            ZipStream.PutNextEntry(entry);
        }

        public void Finish()
        {
            if (!ZipStream.IsFinished)
            {
                ZipStream.Finish();
            }
        }

        public void Close()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing && ZipStream != null)
                ZipStream.Dispose();
            _disposed = true;
        }

        public static void ExtractToFolder(string zipFilePath, string outputFolder)
        {
            using (FileStream stream = File.OpenRead(zipFilePath))
            {
                using (var s = new ZipInputStream(stream))
                {
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        var data = new byte[s.Length];
                        s.Read(data, 0, data.Length);
                        File.WriteAllBytes(Path.Combine(outputFolder, theEntry.Name), data);
                    }
                }
            }
        }
    }
}