﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace AllTrades.Files {
    /// <summary>
    /// Description of ODCConnectionFile.
    /// </summary>
    public class ODCConnectionFile {
        public static readonly XNamespace ODCNamespace = "urn:schemas-microsoft-com:office:odc";
        public static readonly string DefaultTableName = "Default";
        public static readonly char TokenDelimeter = ';';

        public ODCConnectionFile(string odcFilePath) : this(odcFilePath, null) {}
        public ODCConnectionFile(string odcFilePath, NetworkCredential credentials) {
            ReadODCDocument(odcFilePath, credentials);

            Init();
        }
        public ODCConnectionFile(Stream odcFileStream) {
            ReadODCDocument(odcFileStream);
            Init();
        }
        public XDocument ODCDocument { get; private set; }
        public string ConnectionType { get; set; }
        public string ConnectionString { get; set; }
        public string CommandText { get; set; }
        public string ODCCommandType { get; set; }
        private void ReadODCDocument(Stream odcFileStream) {
            using (var sr = new StreamReader(odcFileStream)) {
                string data = sr.ReadToEnd();
                int sourceIndex = data.IndexOf("<odc:OfficeDataConnection");
                int destinationIndex = data.IndexOf("</odc:OfficeDataConnection");
                ODCDocument = XDocument.Parse(data.Substring(sourceIndex, destinationIndex - sourceIndex + 27));
                sr.Close();
            }
        }
        private void ReadODCDocument(string odcFilePath, NetworkCredential credentials) {
            if (odcFilePath.StartsWith("http") && odcFilePath.Contains("://")) {
                using (var wc = new WebClient()) {
                    wc.Credentials = credentials;
                    wc.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                    using (var ms = new MemoryStream(wc.DownloadData(odcFilePath))) {
                        ReadODCDocument(ms);
                        ms.Close();
                    }
                }
            } else {
                using (Stream ms = new FileStream(odcFilePath, FileMode.Open)) {
                    ReadODCDocument(ms);
                    ms.Close();
                }
            }
        }
        private void Init() {
            XElement xConn = ODCDocument.Root.Element(ODCNamespace + "Connection");
            ConnectionType = xConn.Attribute(ODCNamespace + "Type").Value;
            ConnectionString = xConn.Element(ODCNamespace + "ConnectionString").Value;
            CommandText = xConn.Element(ODCNamespace + "CommandText").Value;
            try {
                ODCCommandType = ODCDocument.Root.Element(ODCNamespace + "Connection").Element(ODCNamespace + "CommandType").Value;
                if (ODCCommandType == "List") {
                    //ODCCommandType = "SharePoint";
                    CommandText = HttpUtility.HtmlDecode(CommandText);
                    //var xCommand = XDocument.Load(CommandText);
                }
            } catch (Exception) {}
        }
        private XElement[] GetParameters() {
            try {
                XElement[] p = ODCDocument.Root
                    .Element(ODCNamespace + "Connection")
                    .Elements(ODCNamespace + "Parameter").ToArray();
                if (p.Length == 0) {
                    return null;
                }
                return p;
            } catch {
                return null;
            }
        }
        public DataSet Execute() {
            return Execute(null);
        }
        public DataSet Execute(object[] parameters) {
            return Execute(parameters, null, null);
        }

        public DataSet Execute(object[] parameters, string[] commandTokens, string[] commandTokenValues) {
            DbConnection connection = null;
            DbDataAdapter adapter = null;

            XElement[] xParameters = GetParameters();
            if (xParameters == null && parameters != null) {
                throw new ArgumentException(
                    "Parameter values specified, but no parameters defined in the connection file");
            }
            if (xParameters != null && parameters == null) {
                throw new ArgumentException("Parameter values were excepted.");
            }
            if (xParameters != null && parameters != null && xParameters.Length != parameters.Length) {
                throw new ArgumentException(
                    "The number of parameter values specified does not match the number of parameters in the connection file");
            }
            if (commandTokens != null && commandTokenValues == null) {
                throw new ArgumentException(
                    "Command Tokens specified without the values to replace");
            }
            if (commandTokens == null && commandTokenValues != null)
            {
                throw new ArgumentException(
                    "Command Token Values specified without the tokens to replace");
            }
            if (commandTokens != null && commandTokenValues != null && commandTokens.Length != commandTokenValues.Length)
            {
                throw new ArgumentException(
                    "Command Tokens does not match specified values");
            }
            string cmdText = CommandText;
            if (commandTokens != null) {
                for (int i = 0; i < xParameters.Length; i++) {
                    cmdText = cmdText.Replace(commandTokens[i], commandTokenValues[i]);
                }
            }
            switch (ConnectionType) {
                case "ODBC":
                    connection = new OdbcConnection(ConnectionString);
                    adapter = new OdbcDataAdapter(cmdText, (OdbcConnection) connection);
                    if (xParameters != null) {
                        for (int i = 0; i < xParameters.Length; i++) {
                            var param = new OdbcParameter {ParameterName = xParameters[i].Element(ODCNamespace + "Name").Value, Value = parameters[i]};
                            //param.DbType = (DbType)(DbType.Parse(typeof(DbType), xParameters[i].Element(ODCConnectionFile.ODCNamespace + "DataType").Value));
                            adapter.SelectCommand.Parameters.Add(param);
                        }
                    }
                    break;
                case "OLEDB":
                    connection = new OleDbConnection(ConnectionString);
                    adapter = new OleDbDataAdapter(cmdText, (OleDbConnection)connection);
                    switch (ODCCommandType) {
                        case "Table":
                            adapter.SelectCommand.CommandType = CommandType.TableDirect;
                            break;
                    }
                    break;
            }

            if (adapter != null) {
                connection.Open();
                var ds = new DataSet();
                adapter.Fill(ds);
                ds.Tables[0].TableName = DefaultTableName;
                return ds;
            }
            return null;
        }
    }
}