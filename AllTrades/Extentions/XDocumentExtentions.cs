﻿using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace AllTrades.Extentions {
    public static class XDocumentExtentions {
        public static byte[] ToByteArray(this XDocument doc) {
            using (Stream stream = new MemoryStream()) {
                using (var writer = XmlWriter.Create(stream)) {
                    doc.Save(writer);
                    writer.Close();
                    stream.Position = 0;
                    var buffer = new byte[stream.Length];
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Flush();
                    stream.Close();
                    return buffer;
                }
            }
        }
    }
}
