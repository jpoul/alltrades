﻿using System.Data;
using System.Linq;

namespace AllTrades.Extentions
{
    public static class DataSetExtentions
    {
        public static void FromUntypedDataTable(this DataTable table, DataTable untypedTable)
        {
            foreach (
                DataRow row in untypedTable.Rows.Cast<DataRow>().Where(row => !string.IsNullOrEmpty(row[0].ToString())))
            {
                table.Rows.Add(row.ItemArray);
            }
        }
    }
}