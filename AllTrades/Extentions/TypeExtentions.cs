﻿using System;
using System.Linq;

namespace AllTrades.Extentions
{
    public static class TypeExtentions
    {
        public static TAttribute GetSingleAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            object attribute = type.GetCustomAttributes(typeof (TAttribute), true).SingleOrDefault();
            if (null == attribute)
            {
                throw new ArgumentException(String.Format("No attribute of type {0} found for type {1}.",
                                                          typeof (TAttribute).Name, type.Name));
            }
            return (TAttribute) attribute;
        }
    }
}