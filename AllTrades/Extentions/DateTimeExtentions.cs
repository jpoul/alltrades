﻿using System;

namespace AllTrades.Extentions
{
    public static class DateTimeExtentions
    {
        public static DateTime RoundToSeconds(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
        }
    }
}