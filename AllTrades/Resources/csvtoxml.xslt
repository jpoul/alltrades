﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:str="http://exslt.org/strings" extension-element-prefixes="str">
  <xsl:param name="hasHeaders" />

  <xsl:output method="xml" indent="yes" encoding="utf-8" version="1.0" omit-xml-declaration="no" />

  <xsl:template match="/">
    <xsl:variable name="lines" select="TextFile/Line[@lineno &gt; 1]" />
    <xsl:variable name="header" select="TextFile/Line[@lineno = 1]" />
    <xsl:variable name="headers" select="str:tokenize($header, ',')" />
    <xsl:element name="CSVFile">
      <xsl:for-each select="$lines">
        <Line>
          <xsl:variable name="cells" select="str:tokenize(., ',')" />
          <xsl:for-each select="$headers">
            <xsl:variable name="pos" select="position()" />
            <xsl:element name="Field">
              <xsl:attribute name="position">
                <xsl:value-of select="$pos" />
              </xsl:attribute>
              <xsl:attribute name="name">
                <xsl:value-of select="." />
              </xsl:attribute>
              <xsl:attribute name="value">
                <xsl:value-of select="$cells[$pos]" />
              </xsl:attribute>
            </xsl:element>
          </xsl:for-each>
        </Line>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>

<!--<xsl:element name="{$headers[position()]}">
								<xsl:attribute name="lineno">
									<xsl:value-of select="position()"/>
								</xsl:attribute>
								<xsl:value-of select="."/>
							</xsl:element>-->