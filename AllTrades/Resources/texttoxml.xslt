﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl" xmlns:str="http://exslt.org/strings" extension-element-prefixes="str">
  <xsl:output method="xml" indent="yes" encoding="utf-8" version="1.0" omit-xml-declaration="no" />
  <xsl:template match="/">
    <xsl:variable name="lines" select="str:tokenize(., '&#xA;')[. != '']" />
    <xsl:element name="TextFile">
      <xsl:attribute name="linecount">
        <xsl:value-of select="count($lines)" />
      </xsl:attribute>
      <xsl:for-each select="$lines">
        <xsl:element name="Line">
          <xsl:attribute name="lineno">
            <xsl:value-of select="position()" />
          </xsl:attribute>
          <!--<xsl:value-of select="normalize-space(.)"/>-->
          <xsl:value-of select="str:replace(normalize-space(.), '&quot;', '')" />
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>