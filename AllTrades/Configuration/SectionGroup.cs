﻿using System;
using System.Configuration;

namespace AllTrades.SharePoint.Utils {
    public class SectionGroup : ISectionGroupElement {
        public string Name { get; set; }
        public Type Type { get; set; }

        #region ISectionGroupElement Members

        public SectionGroup Parent { get; set; }

        #endregion
    }

    public class SectionGroup<T> : SectionGroup where T : ConfigurationSectionGroup {
        public SectionGroup() {
            Type = typeof (T);
        }
    }
}