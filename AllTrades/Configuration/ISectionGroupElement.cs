﻿namespace AllTrades.SharePoint.Utils {
    public interface ISectionGroupElement {
        SectionGroup Parent { get; set; }
    }
}