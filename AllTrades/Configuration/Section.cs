﻿using System;
using System.Configuration;

namespace AllTrades.SharePoint.Utils {
    public class Section : ISectionGroupElement {
        public string Name { get; set; }
        public Type Type { get; set; }
        public bool RequirePermission { get; set; }
        public ConfigurationAllowDefinition AllowDefinition { get; set; }

        #region ISectionGroupElement Members

        public SectionGroup Parent { get; set; }

        #endregion
    }

    public class Section<T> : Section where T : ConfigurationSection {
        public Section() {
            Type = typeof (T);
        }
    }
}