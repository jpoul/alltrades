﻿using System.IO;
using System.Net;

namespace AllTrades.Utils
{
    public static class WebUtils
    {
        /// <summary>
        /// downloads a file over http using a GET request with default credentials
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Stream GetHttpFileUsingDefaultCredentials(string url)
        {
            return GetHttpFileWCredentials(url, CredentialCache.DefaultNetworkCredentials);
        }

        /// <summary>
        /// downloads a file over http using a GET request with custom credentials
        /// </summary>
        /// <param name="url"></param>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public static Stream GetHttpFileWCredentials(string url, ICredentials credentials)
        {
            WebRequest myFileDl = WebRequest.Create(url);

            myFileDl.Credentials = credentials;

            WebResponse myWr = myFileDl.GetResponse();

            return myWr.GetResponseStream();
        }
    }
}