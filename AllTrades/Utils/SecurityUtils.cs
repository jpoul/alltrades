﻿using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace AllTrades.Utils
{
    public static class SecurityUtils
    {
        public static SecureString ToSecureString(string text)
        {
            char[] passwordChars = text.ToCharArray();
            var password = new SecureString();
            foreach (char c in passwordChars)
            {
                password.AppendChar(c);
            }
            return password;
        }

        public static string GenerateSalt()
        {
            var data = new byte[16];
            new RNGCryptoServiceProvider().GetBytes(data);
            return Convert.ToBase64String(data);
        }

        public static byte[] EncodePasswordWithSalt(string pass, string salt)
        {
            byte[] bIn = Encoding.Unicode.GetBytes(pass);
            byte[] bSalt = Convert.FromBase64String(salt);
            var bAll = new byte[bSalt.Length + bIn.Length];
            Buffer.BlockCopy(bSalt, 0, bAll, 0, bSalt.Length);
            Buffer.BlockCopy(bIn, 0, bAll, bSalt.Length, bIn.Length);
            return bAll;
        }
    }
}