﻿using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;
using AllTrades.Properties;
using Mvp.Xml.Common.Xsl;

namespace AllTrades.Utils
{
    /// <summary>
    /// Utility class for common xsl utilities, transformations and tasks
    /// </summary>
    //TODO: XslUtils should use XDocument
    public static class XslUtils
    {
        public static XmlDocument TransformCsvToXml(Stream textFileStream, bool hasHeaders)
        {
            return TransformCsvToXml(textFileStream, Encoding.Default, hasHeaders);
        }

        public static XmlDocument TransformCsvToXml(string fileName, bool hasHeaders)
        {
            return TransformCsvToXml(fileName, Encoding.Default, hasHeaders);
        }

        public static XmlDocument TransformCsvToXml(string fileName, Encoding encoding, bool hasHeaders)
        {
            var textFileStream = new StreamReader(fileName, encoding);
            return TransformCsvToXml(textFileStream.BaseStream, encoding, hasHeaders);
        }

        public static XmlDocument TransformCsvToXml(Stream textFileStream, Encoding encoding, bool hasHeaders)
        {
            XmlDocument docResult = TransformTextToXml(textFileStream, encoding);
            var docXslt = new XmlDocument();

            docXslt.LoadXml(Resources.csvtoxml);
            var argList = new XsltArgumentList();
            argList.AddParam("hasHeaders", "", hasHeaders.ToString());

            return TransformToXml(docResult, docXslt);
        }

        public static XmlDocument TransformTextToXml(Stream textFileStream)
        {
            return TransformTextToXml(textFileStream, Encoding.Default);
        }

        public static XmlDocument TransformTextToXml(string fileName)
        {
            return TransformTextToXml(fileName, Encoding.Default);
        }

        public static XmlDocument TransformTextToXml(string fileName, Encoding encoding)
        {
            var textFileStream = new StreamReader(fileName, encoding);
            return TransformTextToXml(textFileStream.BaseStream, encoding);
        }

        public static XmlDocument TransformTextToXml(Stream textFileStream, XmlDocument xsltDocument)
        {
            return TransformTextToXml(textFileStream, Encoding.Default, xsltDocument);
        }

        public static XmlDocument TransformTextToXml(Stream textFileStream, Encoding encoding, XmlDocument xsltDocument)
        {
            XmlDocument docText = TransformTextToXml(textFileStream, encoding);
            return TransformToXml(docText, xsltDocument);
        }

        public static XmlDocument TransformTextToXml(string fileName, Encoding encoding, string xsltFileName)
        {
            var docXslt = new XmlDocument();
            docXslt.Load(xsltFileName);
            var textFileStream = new StreamReader(fileName, encoding);
            return TransformTextToXml(textFileStream.BaseStream, encoding, docXslt);
        }

        public static XmlDocument TransformTextToXml(Stream textFileStream, Encoding encoding, Stream xsltDocumentStream)
        {
            var docXslt = new XmlDocument();
            docXslt.Load(xsltDocumentStream);
            return TransformTextToXml(textFileStream, encoding, docXslt);
        }

        public static XmlDocument TransformTextToXml(Stream textFileStream, Encoding encoding)
        {
            var docXslt = new XmlDocument();
            var docText = new XmlDocument();
            var docOutput = new XmlDocument();
            docXslt.LoadXml(Resources.texttoxml);

            XmlDeclaration textDec = docText.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlDeclaration outputDec = docOutput.CreateXmlDeclaration("1.0", "utf-8", null);
            docText.AppendChild(textDec);
            docOutput.AppendChild(outputDec);


            XmlNode rootNode = docText.CreateNode(XmlNodeType.Element, "TextFile", "");
            using (var rd = new StreamReader(textFileStream, encoding))
            {
                //TODO: Get rid of System.Web
                rootNode.InnerXml = HttpUtility.HtmlEncode(rd.ReadToEnd());
            }
            docText.LoadXml(rootNode.OuterXml);

            var xs = new MvpXslTransform();
            xs.Load(docXslt);

            XmlReader result = xs.Transform(new XmlInput(docText), null);
            docOutput.Load(result);
            return docOutput;
        }

        public static XmlDocument TransformToXml(XmlDocument xmlDocument, XmlDocument xsltDocument)
        {
            return TransformToXml(xmlDocument, null, xsltDocument);
        }

        public static XmlDocument TransformToXml(XmlDocument xmlDocument, XsltArgumentList arguments,
                                                 XmlDocument xsltDocument)
        {
            var docOutput = new XmlDocument();
            XmlDeclaration outputDec = docOutput.CreateXmlDeclaration("1.0", "utf-8", null);
            docOutput.AppendChild(outputDec);
            var xs = new MvpXslTransform();
            xs.Load(xsltDocument);
            XmlReader result = xs.Transform(new XmlInput(xmlDocument), arguments);
            docOutput.Load(result);
            return docOutput;
        }

        public static XmlWriter CreateDefaultXmlWriter()
        {
            var stream = new MemoryStream();
            var settings = new XmlWriterSettings {Encoding = Encoding.UTF8, OmitXmlDeclaration = false, Indent = true};
            return XmlWriter.Create(stream, settings);
        }
    }
}