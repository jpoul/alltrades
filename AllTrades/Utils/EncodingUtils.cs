﻿using System.IO;
using System.Text;

namespace AllTrades.Utils
{
    public static class EncodingUtils
    {
        public static void EncodeFile(string inputFile, string outputFile, Encoding sourceEncoding,
                                      Encoding targetEncoding, bool append)
        {
            using (var reader = new StreamReader(inputFile, sourceEncoding))
            {
                using (var writer = new StreamWriter(outputFile, append, targetEncoding))
                {
                    writer.WriteLine(reader.ReadToEnd());
                }
            }
        }
    }
}