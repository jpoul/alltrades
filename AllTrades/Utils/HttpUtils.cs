﻿using System;
using System.IO;
using System.Web;

namespace AllTrades.Utils
{
    public static class HttpUtils
    {
        public static void WriteStreamToResponse(Stream ms, HttpResponse response) {
            WriteStreamToResponse(ms, response, null);
        }
        public static void WriteStreamToResponse(Stream ms, HttpResponse response, string fileName)
        {
            if (ms.Length <= 0)
                return;

            var filenameForResponse = fileName == null ? DateTime.Now.ToFileTime() + ".zip" : fileName;
            response.Clear();
            response.ClearHeaders();
            response.ClearContent();
            response.AddHeader("Content-Length", ms.Length.ToString());
            response.AddHeader("Content-Disposition", "attachment; filename=" + filenameForResponse);
            response.ContentType = "application/octet-stream";

            var buffer = new byte[65536];
            ms.Position = 0;
            int num;
            do
            {
                num = ms.Read(buffer, 0, buffer.Length);
                response.OutputStream.Write(buffer, 0, num);
            }

            while (num > 0);

            response.Flush();
        }
    }
}
