namespace AllTrades.Enums
{
    public enum XslTransformationType
    {
        XmlToXml = 0,
        TextToXml,
        CsvToXml
    }
}