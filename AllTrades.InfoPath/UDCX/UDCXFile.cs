﻿using System.IO;
using System.Xml.Linq;
using AllTrades.InfoPath.Properties;

namespace AllTrades.InfoPath.UDCX {
    public abstract class UDCXFile {
        public static readonly XNamespace InfoPathDataConnectionNs = "http://schemas.microsoft.com/office/infopath/2006/udc";
        public const string InfopathDataSourceContentTypeId = "0x010100B4CBD48E029A4ad8B62CB0E41868F2B0";
        public XDocument Xml { get; protected set; } 
        public static XDocument GetUDCXTemplate() {
            using (Stream s = new MemoryStream(Resources.Template)) {
                using (TextReader r = new StreamReader(s)) {
                    return XDocument.Load(r);
                }
            }
        }

        protected UDCXFile() {
            Xml = GetUDCXTemplate();
        }

        public abstract void UpdateXml();
    }
}