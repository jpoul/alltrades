﻿using System.Collections;
using System.IO;
using System.Linq;
using System.Text;

namespace AllTrades.InfoPath {
    public static class Utils {
        public static void ReplaceInInfoPathFile(string infoPathFile, Hashtable texts, string newFileName, string tempFolder, bool clearTemp) {
            if (!File.Exists(infoPathFile)) {
                return;
            }
            if (Directory.Exists(tempFolder)) {
                Directory.Delete(tempFolder, true);
            }
            Directory.CreateDirectory(tempFolder);
            var extractor = new CabLib.Extract();
            extractor.ExtractFile(infoPathFile, tempFolder);
            ReplaceInInfoPathPublishedFolder(tempFolder, texts, newFileName);
            if (clearTemp) {
                Directory.Delete(tempFolder, true);
            }
        }

        public static void ReplaceInInfoPathPublishedFolder(string infoPathPublishedFolder, Hashtable texts, string newFileName) {
            var manifestFile = Path.Combine(infoPathPublishedFolder, "manifest.xsf");
            string contents = File.ReadAllText(manifestFile, Encoding.UTF8);
            contents = texts.Cast<DictionaryEntry>().Aggregate(contents, (current, text) => current.Replace(text.Key.ToString(), text.Value.ToString()));

            File.WriteAllText(manifestFile, contents, Encoding.UTF8);
            var compressor = new CabLib.Compress();
            if (File.Exists(newFileName)) {
                File.Delete(newFileName);
            }
            compressor.CompressFolder(infoPathPublishedFolder, newFileName, null, false, false, 0);
        }
    }
}
