﻿namespace AllTrades.InfoPath {
    public enum DataSourceFilePurpose {
        ReadOnly,
        WriteOnly,
        ReadWrite
    }
}