﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Configuration;
using AllTrades.Enums;
using AllTrades.SharePoint.Utils;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace AllTrades.SharePoint.Farm.Configuration {
    public class SPWebConfigModificationHelper 
    {
        public SPWebConfigModificationHelper(SPWebApplication webApp, string owner) {
            Owner = owner;
            WebApp = webApp;
        }

        public SPWebConfigModificationHelper(SPWebApplication webApp, SPFeatureReceiverProperties properties)
            : this(webApp, properties.Definition.Id.ToString()) {}

        public SPWebConfigModificationHelper(SPFeatureReceiverProperties properties)
            : this(((SPWebApplication)properties.Feature.Parent), properties.Definition.Id.ToString()) { }


        #region Modifiers

        public void Modify(SPWebConfigModification modification, SPWebConfigModificationType type) {
            switch (type) {
                case SPWebConfigModificationType.Add:
                    WebConfigModifications.Add(modification);
                    break;
                case SPWebConfigModificationType.Remove:
                    WebConfigModifications.Remove(modification);
                    break;
            }
        }

        public void ModifyCompilationAssembly(string assemblyName, SPWebConfigModificationType type) {
            Modify(CreateCompilationAssemblyModification(assemblyName), type);
        }

        public void ModifyCompilationAssembly(Assembly assembly, SPWebConfigModificationType type) {
            ModifyCompilationAssembly(assembly.FullName, type);
        }

        public void ModifyHttpModule<T>(string name, IisVersion iisVersion, WebConfigEntryActionType actionType,
                                        SPWebConfigModificationType type) where T : IHttpModule {
            Modify(CreateHttpModuleModification<T>(name, iisVersion), type);
        }

        public void ModifyHttpHandler(HttpHandlerAction handler, string name, IisVersion iisVersion, WebConfigEntryActionType actionType,
                                      SPWebConfigModificationType type) {
            Modify(CreateHttpHandlerModification(handler, name, iisVersion), type);
        }

        public void ModifyAppSetting(string key, string value, SPWebConfigModificationType type) {
            Modify(CreateAppSettingModification(key, value), type);
        }

        public void ModifySectionGroup(SectionGroup sectionGroup, SPWebConfigModificationType type) {
            Modify(CreateSectionGroupModification(sectionGroup), type);
        }

        public void ModifySection(Section section, SPWebConfigModificationType type) {
            Modify(CreateSectionModification(section), type);
        }

        public void ModifySafeControl(Assembly assembly, string nameSpace, string typeName, bool isSafe,
                                      SPWebConfigModificationType type) {
            Modify(CreateSafeControlModification(assembly, nameSpace, typeName, isSafe), type);
        }

        public void ModifyAuthorizedType(Assembly assembly, string nameSpace, string typeName, bool isSafe,
                                         SPWebConfigModificationType type) {
            Modify(CreateAuthorizedTypeModification(assembly, nameSpace, typeName, isSafe), type);
        }

        public void EnsureControlsSection() {
            Modify(CreateControlsSectionModification(), SPWebConfigModificationType.Add);
        }

        public void ModifyControl(string tagPrefix, string nameSpace, Assembly assembly,
                                  SPWebConfigModificationType type) {
            Modify(CreateControlModification(tagPrefix, nameSpace, assembly.FullName), type);
        }

        public void ModifyControl(string tagPrefix, string nameSpace, string assemblyName,
                                  SPWebConfigModificationType type) {
            Modify(CreateControlModification(tagPrefix, nameSpace, assemblyName), type);
        }

        public void ModifyDependentAssembly(string name, string publicKeyToken, string oldVersion, string newVersion,
                                            SPWebConfigModificationType type) {
            Modify(CreateDependentAssemblyModification(name, publicKeyToken, oldVersion, newVersion), type);
        }

        public void ModifyRootSection(string name, string value, SPWebConfigModificationType type) {
            Modify(CreateRootModification(name, value), type);
        }

        /// <summary>
        /// Remove any modifications that were originally created by the owner
        /// </summary>
        public void RemoveAllOwnedModifications() {
            for (int i = WebConfigModifications.Count - 1; i >= 0; i--) {
                SPWebConfigModification modification = WebConfigModifications[i];
                if (modification.Owner == Owner)
                    WebConfigModifications.Remove(modification);
            }
        }

        #endregion

        public string Owner { get; private set; }
        public SPWebApplication WebApp { get; private set; }

        private Collection<SPWebConfigModification> WebConfigModifications {
            get { return WebApp.WebConfigModifications; }
        }

        public void SaveChanges() {
                        WebApp.Update();
            WebApp.Farm.Services.GetValue<SPWebService>().ApplyWebConfigModifications();

        }

       #region Modification Builders

        public SPWebConfigModification CreateCompilationAssemblyModification(string assemblyName) {
            string modName = "add[@assembly='" + assemblyName + "']";
            const string modXPath = "configuration/system.web/compilation/assemblies";
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = string.Format(@"<add assembly=""{0}"" />", assemblyName)
                                                                  };
        }

        public SPWebConfigModification CreateAppSettingModification(string key, string value) {
            string modName = "add[@key='" + key + "']";
            const string modXPath = "configuration/appSettings";
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = string.Format(@"<add key=""{0}"" value=""{1}"" />",
                                                                                            key,
                                                                                            value)
                                                                  };
        }

        private static void BuildSectionGroupXPath(StringBuilder modXPath, ISectionGroupElement element) {
            if (element.Parent == null)
                return;

            SectionGroup parent = element.Parent;
            modXPath.Insert(0, string.Format("/sectionGroup[@name='{0}']", parent.Name));

            BuildSectionGroupXPath(modXPath, parent);
        }

        public SPWebConfigModification CreateSectionGroupModification(SectionGroup sectionGroup) {
            string modName = @"sectionGroup[@name='" + sectionGroup.Name + "']";
            var modXPath = new StringBuilder();
            BuildSectionGroupXPath(modXPath, sectionGroup);
            modXPath.Insert(0, "configuration/configSections");

            return new SPWebConfigModification(modName, modXPath.ToString()) {
                                                                                 Owner = Owner,
                                                                                 Sequence = 0,
                                                                                 Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                                 Value = string.Format(@"<sectionGroup name=""{0}"" type=""{1}"" />",
                                                                                                       sectionGroup.Name,
                                                                                                       sectionGroup.Type.AssemblyQualifiedName)
                                                                             };
        }

        public SPWebConfigModification CreateSectionModification(Section section) {
            string modName = @"section[@name='" + section.Name + "']";
            var modXPath = new StringBuilder();
            BuildSectionGroupXPath(modXPath, section);
            modXPath.Insert(0, "configuration/configSections");

            return new SPWebConfigModification(modName, modXPath.ToString()) {
                                                                                 Owner = Owner,
                                                                                 Sequence = 0,
                                                                                 Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                                 Value =
                                                                                     string.Format(
                                                                                         @"<section name=""{0}"" type=""{1}"" requirePermission=""{2}"" allowDefinition=""{3}"" />",
                                                                                         section.Name,
                                                                                         section.Type.AssemblyQualifiedName,
                                                                                         section.RequirePermission.ToString().ToLower(),
                                                                                         section.AllowDefinition)
                                                                             };
        }

        public SPWebConfigModification CreateSafeControlModification(Assembly assembly, string nameSpace,
                                                                     string typeName, bool isSafe) {
            string modName = @"SafeControl[@Assembly='" + assembly.FullName + "' and @Namespace='" + nameSpace +
                             "' and @TypeName='" + typeName + "']";
            const string modXPath = "configuration/SharePoint/SafeControls";

            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value =
                                                                          string.Format(
                                                                              @"<SafeControl Assembly=""{0}"" Namespace=""{1}"" TypeName=""{2}"" Safe=""{3}"" />",
                                                                              assembly.FullName,
                                                                              nameSpace,
                                                                              typeName,
                                                                              isSafe)
                                                                  };
        }

        public SPWebConfigModification CreateAuthorizedTypeModification(Assembly assembly, string nameSpace,
                                                                        string typeName, bool isSafe) {
            string modName = @"authorizedType[@Assembly='" + assembly.FullName + "' and @Namespace='" + nameSpace +
                             "' and @TypeName='" + typeName + "']";
            const string modXPath = "configuration/System.Workflow.ComponentModel.WorkflowCompiler/authorizedTypes";

            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value =
                                                                          string.Format(
                                                                              @"<authorizedType Assembly=""{0}"" Namespace=""{1}"" TypeName=""{2}"" Authorized=""{3}"" />",
                                                                              assembly.FullName,
                                                                              nameSpace,
                                                                              typeName,
                                                                              isSafe)
                                                                  };
        }
        
        public SPWebConfigModification CreateHttpHandlerModification(HttpHandlerAction handler, string name, IisVersion iisVersion) {
            string modName = string.Format("add[@type='{0}']", handler.Type) ;
            StringBuilder value = new StringBuilder()
                .AppendFormat(@"<add verb=""{0}"" path=""{1}""", handler.Verb, handler.Path);
            if (iisVersion == IisVersion.Iis7)
                value.AppendFormat(@" name=""{0}""", name);
            if (iisVersion == IisVersion.Iis6)
                value.AppendFormat(@" validate=""{0}""", handler.Validate);
            value.AppendFormat(@" type=""{0}""", handler.Type);
            value.Append(" />");

           string modXPath = string.Format("configuration/{0}/{1}",
                                            (iisVersion == IisVersion.Iis6 ? "system.web" : "system.webServer"),
                                            (iisVersion == IisVersion.Iis6 ? "httpHandlers" : "handlers"));
            
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = value.ToString()
                                                                  };
        }

        public SPWebConfigModification CreateHttpModuleModification<T>(string name, IisVersion iisVersion)
            where T : IHttpModule {
            string modName = string.Format("add[@type='{0}']", typeof(T).AssemblyQualifiedName);
            string modXPath = string.Format("configuration/{0}/{1}",
                                            (iisVersion == IisVersion.Iis6 ? "system.web" : "system.webServer"),
                                            (iisVersion == IisVersion.Iis6 ? "httpModules" : "modules"));

            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = string.Format(@"<add name=""{0}"" type=""{1}"" />", name, typeof (T).AssemblyQualifiedName)
                                                                  };
        }

        private SPWebConfigModification CreateControlsSectionModification() {
            const string modName = "controls";
            const string modXPath = "configuration/system.web/pages";
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = GetType().AssemblyQualifiedName,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = "<controls />"
                                                                  };
        }

        public SPWebConfigModification CreateControlModification(string tagPrefix, string nameSpace, string assemblyName) {
            string modName = string.Format("add[@tagPrefix='{0}' and @namespace='{1}']", tagPrefix, nameSpace);
            const string modXPath = "configuration/system.web/pages/controls";
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value =
                                                                          string.Format(@"<add tagPrefix=""{0}"" namespace=""{1}"" assembly=""{2}"" />", tagPrefix,
                                                                                        nameSpace, assemblyName)
                                                                  };
        }

        public SPWebConfigModification CreateDependentAssemblyModification(string name, string publicKeyToken,
                                                                           string oldVersion, string newVersion) {
            string modName = string.Format(
                "*[local-name()='dependentAssembly'][*/@name='{0}'][*/@publicKeyToken='{1}']", name, publicKeyToken);
            const string modXPath =
                "configuration/runtime/*[local-name()='assemblyBinding' and namespace-uri()='urn:schemas-microsoft-com:asm.v1']";
            return new SPWebConfigModification(modName, modXPath) {
                                                                      Owner = Owner,
                                                                      Sequence = 0,
                                                                      Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                      Value = string.Format(
                                                                          "<dependentAssembly><assemblyIdentity name='{0}' publicKeyToken='{1}' /><bindingRedirect oldVersion='{2}' newVersion='{3}' /></dependentAssembly>",
                                                                          name,
                                                                          publicKeyToken,
                                                                          oldVersion,
                                                                          newVersion)
                                                                  };
        }

        public SPWebConfigModification CreateRootModification(string name, string value) {
            const string modXPath = "configuration";
            return new SPWebConfigModification(name, modXPath) {
                                                                   Owner = Owner,
                                                                   Sequence = 0,
                                                                   Type = SPWebConfigModification.SPWebConfigModificationType.EnsureChildNode,
                                                                   Value = value
                                                               };
        }

        #endregion

        public static void RemoveFeatureModifications(SPFeatureReceiverProperties properties) {
            var modificationHelper = new SPWebConfigModificationHelper(((SPWebApplication)properties.Feature.Parent), properties);
            modificationHelper.RemoveAllOwnedModifications();
            modificationHelper.SaveChanges();
        }

        public static void AddFeatureModifications(SPFeatureReceiverProperties properties, Func<SPWebConfigModificationHelper, IEnumerable<SPWebConfigModification>> modifications)
        {
            var modificationHelper = new SPWebConfigModificationHelper(properties);
            modificationHelper.RemoveAllOwnedModifications();
            foreach (var modification in modifications(modificationHelper))
            {
                modificationHelper.Modify(modification, SPWebConfigModificationType.Add);
            }
            modificationHelper.SaveChanges();
        }

    }
}