﻿namespace AllTrades.SharePoint.Utils {
    public enum SPWebConfigModificationType {
        Add,
        Remove
    }
}