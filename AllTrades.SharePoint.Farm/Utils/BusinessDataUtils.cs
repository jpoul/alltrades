﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.BusinessData.MetadataModel;
using Microsoft.BusinessData.Runtime;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.BusinessData.SharedService;
using Microsoft.SharePoint.Utilities;

namespace AllTrades.SharePoint.Farm.Utils {
    public static class BusinessDataUtils {
        
        public static IEntity GetEntity(string nameSpace, string entityName, SPServiceContext serviceContext) {
            var service = SPFarm.Local.Services.GetValue<BdcService>();
            IMetadataCatalog catalog = service.GetDatabaseBackedMetadataCatalog(serviceContext);
            return catalog.GetEntity(nameSpace, entityName);
        }

        public static IEntityInstance FindEntityInstanceById(string contextSiteUrl, string nameSpace, string entityName, object id) {
            IEntityInstance entityInstance = null;
            SPSecurity.RunWithElevatedPrivileges(
                () => {
                    using (var site = new SPSite(contextSiteUrl)) {
                        using (new SPServiceContextScope(SPServiceContext.GetContext(site))) {
                            var ent = GetEntity(nameSpace, entityName, SPServiceContext.Current);
                            var vw = ent.GetDefaultSpecificFinderView();
                            var identifierField = vw.Fields.Where(field => field.TypeDescriptor.ContainsIdentifier).First();
                            var identifierType = identifierField.TypeDescriptor.GetIdentifier().IdentifierType;
                            var convertedId = identifierType.Equals(typeof (Guid))
                                                  ? new Guid(id.ToString())
                                                  : Convert.ChangeType(id, identifierType);
                            var identity = new Identity(convertedId);
                            var lobSystem = ent.GetLobSystem();
                            var namedLobSystemInstanceDictionary = lobSystem.GetLobSystemInstances();
                            var lobSystemInstance = namedLobSystemInstanceDictionary[0].Value;
                            entityInstance = ent.FindSpecific(identity, lobSystemInstance);
                        }
                    }
                });
            return entityInstance;
        }

        public static EntityInstanceReference GetEntityInstanceReference(IEntityInstance instance) {
            return new EntityInstanceReference(instance);
        }

        public static void SetBusinessFieldValue(string fieldName, SPListItem item, IEntityInstance entityInstance) {
            SetBusinessFieldValue(((SPBusinessDataField) item.Fields[fieldName]), item, entityInstance);
        }

        public static void SetBusinessFieldValue(SPBusinessDataField spfield, SPListItem item, IEntityInstance entityInstance)
        {
            var vw = entityInstance.Entity.GetDefaultSpecificFinderView();
            var secondaryFieldNamesBdc = new List<string>(spfield.GetSecondaryFieldsNames());
            var secondaryFieldNamesWss = MultiValPropertyToStrings(spfield, "SecondaryFieldWssNames");
            for (var i = 0; i < secondaryFieldNamesBdc.Count; i++)
            {
                var currentBdcFieldName = secondaryFieldNamesBdc[i];
                var currentWssFieldName = secondaryFieldNamesWss[i];
                var currentBdcFieldNameDecoded = HttpUtility.UrlDecode(currentBdcFieldName);
                foreach (var field in vw.Fields.Where(field => currentBdcFieldNameDecoded == field.Name)) {
                    item[currentWssFieldName] = entityInstance.GetFormatted(field);
                }
            }
            item[spfield.RelatedField] = entityInstance.GetIdentity().ToString();
            spfield.ParseAndSetValue(item, entityInstance.GetFormatted(spfield.BdcFieldName).ToString());
            item.Update();
        }
        
        private static List<string> MultiValPropertyToStrings(SPBusinessDataField field, string propName)
        {
            var list = new List<string>();
            var property = field.GetProperty(propName);
            if (!string.IsNullOrEmpty(property))
            {
                list.AddRange(SplitStrings(property));
            }
            return list;
        }
        
        private static IEnumerable<string> SplitStrings(string combinedEncoded)
        {
            string[] array;
            var list = new ArrayList();
            if ("0" == combinedEncoded)
            {
                return new string[0];
            }
            try
            {
                var str = SPHttpUtility.UrlKeyValueDecode(combinedEncoded);
                var strArray2 = str.Split(new[] { ' ' }, StringSplitOptions.None);
                int result;
                if (!int.TryParse(strArray2[strArray2.Length - 1], NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
                {
                    throw new ArgumentException(string.Empty, "combinedEncoded");
                }
                var num2 = str.LastIndexOf(' ');
                var str2 = str.Substring(result, num2 - result);
                var length = str2.Length;
                var index = 0;
                var startIndex = 0;
                while (startIndex < length)
                {
                    var s = strArray2[index];
                    var num6 = 1;
                    if ((s != null) && (s.Length == 0))
                    {
                        list.Add(null);
                    }
                    else
                    {
                        if (!int.TryParse(s, NumberStyles.Integer, CultureInfo.InvariantCulture, out num6))
                        {
                            throw new ArgumentException(string.Empty, "combinedEncoded");
                        }
                        list.Add(str2.Substring(startIndex, num6 - 1));
                    }
                    startIndex += num6;
                    index++;
                }
                array = new string[list.Count];
                list.CopyTo(array);
            }
            catch (Exception exception)
            {
                throw new ArgumentException(string.Empty, "combinedEncoded", exception);
            }
            return array;
        }

    }
}