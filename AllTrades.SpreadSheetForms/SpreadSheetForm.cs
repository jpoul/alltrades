﻿/*
 * Created by SharpDevelop.
 * User: jpoul
 * Date: 12/07/2012
 * Time: 16:10
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using AllTrades.SpreadSheetForms.Aspose;
using Aspose.Cells;
using Aspose.Cells.Tables;
using DocumentFormat.OpenXml.Packaging;

namespace AllTrades.SpreadSheetForms {
    /// <summary>
    /// Description of SpreadSheetForm.
    /// </summary>
    public class SpreadSheetForm {
        #region Aspose Licence
        public static bool UseAsposeLicence = true;
        protected static License AsposeLicense;
        private static void SetLicence() {
            if (AsposeLicense != null) return;
            if (!UseAsposeLicence) {
                AsposeLicense = null;
                return;
            }

            AsposeLicense = new License();
            AsposeLicense.SetLicense("Aspose.Total.lic");
        }
        #endregion

        public const string DefaultPassword = "p@ssw0rd";
        public SpreadSheetForm() {
            SetLicence();
        }
        public SpreadSheetForm(string spreadSheetFilePath, AllTrades.SpreadSheetForms.SpreadSheetFormDataConfiguration.GetODCConnectionFromFile getConnectionDelegate) : this(spreadSheetFilePath, null, getConnectionDelegate) { }
        public SpreadSheetForm(string spreadSheetFilePath) : this(spreadSheetFilePath, null, null) { }
        public SpreadSheetForm(Stream spreadSheetStream, AllTrades.SpreadSheetForms.SpreadSheetFormDataConfiguration.GetODCConnectionFromFile getConnectionDelegate) : this(spreadSheetStream, null, getConnectionDelegate) { }
        public SpreadSheetForm(Stream spreadSheetStream) : this(spreadSheetStream, null, null) { }
        public SpreadSheetForm(string spreadSheetFilePath, NetworkCredential credentials, AllTrades.SpreadSheetForms.SpreadSheetFormDataConfiguration.GetODCConnectionFromFile getConnectionDelegate)
            : this()
        {
            UserCredentials = credentials;
            SpreadSheet = new Workbook(spreadSheetFilePath);
            using (var fileStream = new FileStream(spreadSheetFilePath, FileMode.Open)) {
                GetDataConfiguration(fileStream, getConnectionDelegate);
            }
        }
        public SpreadSheetForm(Stream spreadSheetStream, NetworkCredential credentials, SpreadSheetFormDataConfiguration.GetODCConnectionFromFile getConnectionDelegate) : this() {
            UserCredentials = credentials;
            SpreadSheet = new Workbook(spreadSheetStream);
            spreadSheetStream.Seek(0, SeekOrigin.Begin);
            GetDataConfiguration(spreadSheetStream, getConnectionDelegate);
        }

        public void GetDataConfiguration(Stream stream) {
            GetDataConfiguration(stream, null);
        }

        public void GetDataConfiguration(Stream stream, SpreadSheetFormDataConfiguration.GetODCConnectionFromFile getConnectionDelegate) {
            DataConfiguration = new List<SpreadSheetFormDataConfiguration>();
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(stream, false)) {
                if (document == null) {
                    throw new ArgumentException("document is null");
                }
                if (document.WorkbookPart.ConnectionsPart != null && document.WorkbookPart.ConnectionsPart.Connections != null) {
                    XDocument xConnections = XDocument.Parse(document.WorkbookPart.ConnectionsPart.Connections.OuterXml);
                    foreach (var sheetPart in document.WorkbookPart.WorksheetParts) {
                        foreach (var table in sheetPart.TableDefinitionParts) {
                            if (table.QueryTableParts != null && table.Table != null) {
                                var qPart = table.QueryTableParts.SingleOrDefault();
                                if (qPart != null && qPart.QueryTable != null && qPart.QueryTable.ConnectionId != null) {
                                    var conId = qPart.QueryTable.ConnectionId.ToString();
                                    var xCon = xConnections.Root.Elements().Where(el => el.Attribute("id") != null && el.Attribute("id").Value == conId).SingleOrDefault();
                                    if (xCon != null && xCon.Attribute("odcFile") != null && xCon.Attribute("name") != null) {
                                        var conf =
                                            new SpreadSheetFormDataConfiguration(xCon.Attribute("odcFile").Value, UserCredentials, getConnectionDelegate) {
                                                                                                                                                              ConnectionName = xCon.Attribute("name").Value,
                                                                                                                                                              TableName = table.Table.Name,
                                                                                                                                                              ConnectionId = conId,
                                                                                                                                                              TableDisplayName = table.Table.DisplayName
                                                                                                                                                          };
                                        DataConfiguration.Add(conf);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public List<SpreadSheetFormDataConfiguration> DataConfiguration { get; private set; }
        public Workbook SpreadSheet { get; private set; }
        public NetworkCredential UserCredentials { get; set; }

        public void Fill() {
            foreach (Worksheet ws in SpreadSheet.Worksheets) {
                foreach (ListObject lo in ws.ListObjects) {
                    ListObject lo1 = lo;
                    SpreadSheetFormDataConfiguration config = DataConfiguration.Where(c => c.TableDisplayName == lo1.DisplayName).SingleOrDefault();
                    if (config == null) continue;
                    DataSet ds = config.Execute();
                    lo.DataRange.ImportDataTable(ds.Tables[0]);
                    lo.UpdateColumnName();
                }
            }
            SpreadSheet.CalculateFormula();
        }

        #region Publishing
        public void PublishTemplate(string fileName) {
            PublishTemplate(fileName, false, DefaultPassword);
        }
        public void PublishTemplate(string fileName, bool protect) {
            PublishTemplate(fileName, protect, DefaultPassword);
        }
        public void PublishTemplate(string fileName, bool protect, string password) {
            using (FileStream fileStream = File.Create(fileName)) {
                PublishTemplate(fileStream, protect, password);
                fileStream.Close();
            }
        }
        public void PublishTemplate(Stream stream, bool protect, string password) {
            if (protect) ProtectForm(password);
            try {
                SpreadSheet.Save(stream, SaveFormat.Xlsx);
                stream.Seek(0, SeekOrigin.Begin);
            } catch (Exception ex) {
                throw new InvalidOperationException("Failed to save the workbook.", ex);
            }
        }
        public void ProtectForm(string password) {
            foreach (Worksheet worksheet in SpreadSheet.Worksheets) {
                ProtectSheet(worksheet.Name, password);
            }
            SpreadSheet.Protect(ProtectionType.All, GetResolvedPassword(password));
        }
        public void UnprotectForm(string password) {
            SpreadSheet.Unprotect(GetResolvedPassword(password));
            foreach (Worksheet worksheet in SpreadSheet.Worksheets) {
                UnprotectSheet(worksheet.Name, password);
            }
        }
        public void ProtectSheet(string sheetName) {
            ProtectSheet(sheetName, DefaultPassword);
        }
        public void ProtectSheet(string sheetName, string password) {
            SpreadSheet.Worksheets[sheetName].Protect(ProtectionType.All, GetResolvedPassword(password), string.Empty);
        }
        public void UnportectSheet(string sheetName) {
            UnprotectSheet(sheetName, DefaultPassword);
        }
        public void UnprotectSheet(string sheetName, string password) {
            SpreadSheet.Worksheets[sheetName].Unprotect(GetResolvedPassword(password));
        }
        private static string GetResolvedPassword(string password) {
            return string.IsNullOrEmpty(password) ? DefaultPassword : password;
        }
        #endregion
    }
}