﻿using System.Data;
using Aspose.Cells;

namespace AllTrades.SpreadSheetForms.Aspose {
	public static class RangeExtentions
	{
		public static DataTable ToDataTable(this Range range, bool hasHeaders, bool isVertical)
		{
			return range.Worksheet.Cells.ExportDataTable(range.FirstRow, range.FirstColumn, range.RowCount, range.ColumnCount, hasHeaders, isVertical);
		}

		public static void ImportDataTable(this Range range, DataTable dataTable)
		{
			range.Worksheet.Cells.InsertRows(range.FirstRow + 1, dataTable.Rows.Count - range.RowCount, true);
			range.Worksheet.Cells.ImportDataTable(dataTable, false, range.FirstRow, range.FirstColumn, false);
		}

		public static int IndexOf(this RangeCollection ranges, string name)
		{
			for (var i = 0; i < ranges.Count; i++)
			{
				if (ranges[i].Name == name)
					return i;
			}
			return -1;
		}


	}
}