﻿using System.Linq;
using Aspose.Cells.Tables;

namespace AllTrades.SpreadSheetForms.Aspose {
    public static class ListObjectsExtentions {
        public static ListObject GetByName (this ListObjectCollection listObjects, string displayName) {
            return listObjects.Cast<ListObject>().Where(lo => lo.DisplayName == displayName).SingleOrDefault();
        }

        public static int IndexOf(this ListObjectCollection listObjects, string displayName) {
            for (var i = 0; i < listObjects.Count; i++) {
                if (listObjects[i].DisplayName == displayName)
                    return i;
            }
            return -1;
        }
	}
}