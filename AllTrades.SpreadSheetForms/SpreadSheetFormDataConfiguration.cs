﻿/*
 * Created by SharpDevelop.
 * User: jpoul
 * Date: 12/07/2012
 * Time: 16:43
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System.Collections.Generic;
using System.Data;
using System.Net;
using AllTrades.Files;

namespace AllTrades.SpreadSheetForms
{
	/// <summary>
	/// Description of SpreadSheetFormDataConfiguration.
	/// </summary>
	public class SpreadSheetFormDataConfiguration
	{
		public string TableName {get;set;}
		public string ConnectionName {get;set;}
		public string ConnectionFile {get; private set;}
		public string ConnectionId {get;set;}
		public string TableDisplayName {get;set;}
		public List<object> Parameters {get; private set;}
		public List<string> TableColumns {get; private set;}
        public List<string> CommandTextTokens { get; private set; }
        public List<string> CommandTextTokenValues { get; private set; }
		public ODCConnectionFile Connection {get; private set;}
		
		public SpreadSheetFormDataConfiguration(string connectionFile) : this(connectionFile, null){}
		public SpreadSheetFormDataConfiguration(string connectionFile, NetworkCredential credentials)
		{
			Parameters = new List<object>();
			TableColumns = new List<string>();
            CommandTextTokens = new List<string>();
            CommandTextTokenValues = new List<string>();
			ConnectionFile=connectionFile;
			Connection = new ODCConnectionFile(ConnectionFile, credentials);
			
		}

	    public delegate ODCConnectionFile GetODCConnectionFromFile(string connectionFileUrl);

        public SpreadSheetFormDataConfiguration(string connectionFile, NetworkCredential credentials, GetODCConnectionFromFile getConnectionDelegate)
        {
            Parameters = new List<object>();
            TableColumns = new List<string>();
            CommandTextTokens = new List<string>();
            CommandTextTokenValues = new List<string>();
            ConnectionFile = connectionFile;
            if (getConnectionDelegate!=null)
                Connection = getConnectionDelegate(ConnectionFile);// new ODCConnectionFile(ConnectionFile, credentials);
            else {
                Connection = new ODCConnectionFile(ConnectionFile, credentials); 
            }
        }

		
		public DataSet Execute() {
			object[] parameters = Parameters.Count > 0
						? Parameters.ToArray()
						: null;
            string[] commandTokens = CommandTextTokens.Count > 0
                                        ? CommandTextTokens.ToArray()
                                        : null;
		    string[] commandTokenValues = CommandTextTokenValues.Count > 0
		                                      ? CommandTextTokenValues.ToArray()
		                                      : null;
			return Connection.Execute(parameters, commandTokens, commandTokenValues);
		}
	}
}
