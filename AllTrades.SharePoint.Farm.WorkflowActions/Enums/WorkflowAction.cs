﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.Enums {
    public enum WorkflowAction {
        CancelErrors,
        RestartErrors,
        Start
    }
}