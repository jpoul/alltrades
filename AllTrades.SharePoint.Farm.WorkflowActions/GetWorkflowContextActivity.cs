using System.Workflow.ComponentModel;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    public class GetWorkflowContextActivity : Activity {
        public static readonly DependencyProperty ContextProperty = DependencyProperty.Register("Context", typeof (WorkflowContext), typeof (GetWorkflowContextActivity));
        public static readonly DependencyProperty WorkflowPropertiesProperty = DependencyProperty.Register("WorkflowProperties", typeof (SPWorkflowActivationProperties), typeof (GetWorkflowContextActivity));
        public GetWorkflowContextActivity() {
            Name = "GetWorkflowContextActivity";
        }
        public WorkflowContext Context {
            get { return (WorkflowContext) GetValue(ContextProperty); }
            set { SetValue(ContextProperty, value); }
        }
        public SPWorkflowActivationProperties WorkflowProperties {
            get { return (SPWorkflowActivationProperties) GetValue(WorkflowPropertiesProperty); }
            set { SetValue(WorkflowPropertiesProperty, value); }
        }
        protected override ActivityExecutionStatus Execute(ActivityExecutionContext executionContext) {
            Context = new WorkflowContext();
            Context.Initialize(WorkflowProperties);
            return ActivityExecutionStatus.Closed;
        }
    }
}