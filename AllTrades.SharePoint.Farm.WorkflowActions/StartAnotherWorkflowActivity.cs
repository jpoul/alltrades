using System;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Compiler;
using AllTrades.SharePoint.Farm.WorkflowActions.Utils;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    /// <summary>
    ///   starts a workflow associated with a list
    /// </summary>
    public class StartAnotherWorkflowActivity : AllTradesActivityBase {
        public static DependencyProperty WorkflowIdentifierProperty = DependencyProperty.Register("WorkflowIdentifier", typeof (string), typeof (StartAnotherWorkflowActivity));
        public static DependencyProperty ContentTypeNameProperty = DependencyProperty.Register("ContentTypeName", typeof (string), typeof (StartAnotherWorkflowActivity));
        [Description("Workflow name or template base id")]
        [ValidationOption(ValidationOption.Required)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string WorkflowIdentifier {
            get { return ((string) (GetValue(WorkflowIdentifierProperty))); }
            set { SetValue(WorkflowIdentifierProperty, value); }
        }
        [Description("Optional Content Type Name")]
        [ValidationOption(ValidationOption.Required)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string ContentTypeName {
            get { return ((string) (GetValue(ContentTypeNameProperty))); }
            set { SetValue(ContentTypeNameProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.AllWebs[__Context.Web.ID]) {
                    SPList list = web.Lists[new Guid(ListId)];
                    SPListItem listItem = list.Items.GetItemById(ListItem);
                    SPWorkflowAssociation association = WorkflowUtils.GetWorkflowAssociation(list, ActivityUtils.ProcessStringField(executionContext, WorkflowIdentifier), ContentTypeName);
                    if (association != null) {
                        site.WorkflowManager.StartWorkflow(listItem, association, association.AssociationData, true);
                    } else {
                        throw new ArgumentException("The specified workflow association was not found.");
                    }
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}