using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AllTrades.SharePoint.Farm.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Features.WorkflowActions {
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>
    [Guid("8d0ed8bb-16bc-437e-98c1-5a629f5fe17e")]
    public class WorkflowActionsEventReceiver : SPFeatureReceiver {
        private static IEnumerable<SPWebConfigModification> GetFeatureModifications(SPWebConfigModificationHelper helper) {
            Type typeOfBase = typeof (AllTradesActivityBase);
            return new[] {
                             helper.CreateAuthorizedTypeModification(typeOfBase.Assembly, typeOfBase.Namespace, "*", true),
                             helper.CreateSafeControlModification(typeOfBase.Assembly, typeOfBase.Namespace, typeOfBase.FullName, true)
                         };
        }
        public override void FeatureActivated(SPFeatureReceiverProperties properties) {
            SPWebConfigModificationHelper.AddFeatureModifications(properties, GetFeatureModifications);
        }
        // Uncomment the method below to handle the event raised before a feature is deactivated.
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties) {
            SPWebConfigModificationHelper.RemoveFeatureModifications(properties);
        }
    }
}