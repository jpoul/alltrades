﻿using System.Workflow.ComponentModel;

namespace AllTrades.SharePoint.Farm.WorkflowActions{
    public partial class LogFaultToHistoryActivity
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.ComponentModel.ActivityBind activitybind1 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind2 = new System.Workflow.ComponentModel.ActivityBind();
            System.Workflow.ComponentModel.ActivityBind activitybind3 = new System.Workflow.ComponentModel.ActivityBind();
            this.faultHandlersActivity1 = new System.Workflow.ComponentModel.FaultHandlersActivity();
            this.TerminateWorkflow = new System.Workflow.ComponentModel.TerminateActivity();
            this.LogStackTrace = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.LogException = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            // 
            // faultHandlersActivity1
            // 
            this.faultHandlersActivity1.Name = "faultHandlersActivity1";
            // 
            // TerminateWorkflow
            // 
            this.TerminateWorkflow.Error = "Workflow terminated due to thrown exception";
            this.TerminateWorkflow.Name = "TerminateWorkflow";
            // 
            // LogStackTrace
            // 
            this.LogStackTrace.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.LogStackTrace.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowError;
            this.LogStackTrace.HistoryDescription = "Exception StackTrace";
            activitybind1.Name = "LogFaultToHistoryActivity";
            activitybind1.Path = "Fault.StackTrace";
            this.LogStackTrace.Name = "LogStackTrace";
            this.LogStackTrace.OtherData = "";
            this.LogStackTrace.UserId = -1;
            this.LogStackTrace.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind1)));
            // 
            // LogException
            // 
            this.LogException.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.LogException.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowError;
            activitybind2.Name = "LogFaultToHistoryActivity";
            activitybind2.Path = "FaultType.Name";
            activitybind3.Name = "LogFaultToHistoryActivity";
            activitybind3.Path = "Fault.Message";
            this.LogException.Name = "LogException";
            this.LogException.OtherData = "";
            this.LogException.UserId = -1;
            this.LogException.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryDescriptionProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind2)));
            this.LogException.SetBinding(Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity.HistoryOutcomeProperty, ((System.Workflow.ComponentModel.ActivityBind)(activitybind3)));
            // 
            // LogFaultToHistoryActivity
            // 
            this.Activities.Add(this.LogException);
            this.Activities.Add(this.LogStackTrace);
            this.Activities.Add(this.TerminateWorkflow);
            this.Activities.Add(this.faultHandlersActivity1);
            this.Name = "LogFaultToHistoryActivity";
            this.CanModifyActivities = false;

        }

        #endregion

        private FaultHandlersActivity faultHandlersActivity1;

        private TerminateActivity TerminateWorkflow;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity LogStackTrace;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity LogException;



    }
}