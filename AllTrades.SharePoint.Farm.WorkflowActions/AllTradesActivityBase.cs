﻿using System;
using System.Workflow.ComponentModel;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    public class AllTradesActivityBase : Activity {
        public static readonly DependencyProperty ListIdProperty = DependencyProperty.Register("ListId", typeof (string), typeof (AllTradesActivityBase));
        public static readonly DependencyProperty ListItemProperty = DependencyProperty.Register("ListItem", typeof (int), typeof (AllTradesActivityBase));
        public static readonly DependencyProperty TraceToHistoryLevelProperty = DependencyProperty.Register("TraceToHistoryLevel", typeof (string), typeof (AllTradesActivityBase));
        public static readonly DependencyProperty __ContextProperty = DependencyProperty.Register("__Context", typeof (WorkflowContext), typeof (AllTradesActivityBase));
        public WorkflowContext __Context {
            get { return (WorkflowContext) GetValue(__ContextProperty); }
            set { SetValue(__ContextProperty, value); }
        }
        public string ListId {
            get { return (string) GetValue(ListIdProperty); }
            set { SetValue(ListIdProperty, value); }
        }
        public int ListItem {
            get { return (int) GetValue(ListItemProperty); }
            set { SetValue(ListItemProperty, value); }
        }
        public string TraceToHistoryLevel {
            get { return (string) GetValue(TraceToHistoryLevelProperty); }
            set { SetValue(TraceToHistoryLevelProperty, value); }
        }
        protected event TraceToHistoryEventHandler TracingToHistory;
        private void InvokeTracingToHistory(TraceToHistoryEventargs e) {
            TraceToHistoryEventHandler handler = TracingToHistory;
            if (handler != null)
                handler(this, e);
        }
        public void TraceToHistory(ActivityExecutionContext executionContext, TraceToHistoryPoint traceToHistoryPoint, Exception exception) {
            var traceToHistoryEventargs = new TraceToHistoryEventargs(traceToHistoryPoint) {Exception = exception};
            InvokeTracingToHistory(traceToHistoryEventargs);
            string hisMessage = traceToHistoryEventargs.Message;
            if (string.IsNullOrEmpty(hisMessage)) {
                switch (traceToHistoryPoint) {
                    case TraceToHistoryPoint.Fault:
                        hisMessage = String.Format("Error: {0}", exception.Message);
                        break;
                    case TraceToHistoryPoint.Start:
                        hisMessage = String.Format("Executing: {0}", GetType().Name);
                        break;
                    case TraceToHistoryPoint.Finish:
                        hisMessage = "Execution completed.";
                        break;
                }
            }

            LogToHistory(executionContext, hisMessage, exception);
        }
        public void LogToHistory(ActivityExecutionContext executionContext, string message) {
            LogToHistory(executionContext, message, null);
        }
        public void LogToHistory(ActivityExecutionContext executionContext, string message, Exception exception) {
            var spService = (ISharePointService) executionContext.GetService(typeof (ISharePointService));
            spService.LogToHistoryList(WorkflowInstanceId, GetSPWorkflowHistoryEventType(exception), -1, TimeSpan.MinValue, GetHistoryOutcome(exception), message, String.Empty);
        }
        public static string GetHistoryOutcome(Exception exception) {
            return exception != null ? "Error" : "Trace";
        }
        public static SPWorkflowHistoryEventType GetSPWorkflowHistoryEventType(Exception exception) {
            return exception == null ? SPWorkflowHistoryEventType.None : SPWorkflowHistoryEventType.WorkflowError;
        }
        protected override ActivityExecutionStatus HandleFault(ActivityExecutionContext executionContext, Exception exception) {
            string.Format("Error : {0}", exception.Message);
            if (TraceToHistoryLevel == "Fault" || TraceToHistoryLevel == "All") {
                TraceToHistory(executionContext, TraceToHistoryPoint.Fault, exception);
            }
            return base.HandleFault(executionContext, exception);
        }
        protected override sealed ActivityExecutionStatus Execute(ActivityExecutionContext executionContext) {
            if (TraceToHistoryLevel == "Start" || TraceToHistoryLevel == "All") {
                TraceToHistory(executionContext, TraceToHistoryPoint.Start, null);
            }
            ActivityExecutionStatus activityExecutionStatus = ExecuteActivityCore(executionContext);
            if (TraceToHistoryLevel == "Result" || TraceToHistoryLevel == "All") {
                TraceToHistory(executionContext, TraceToHistoryPoint.Finish, null);
            }
            return activityExecutionStatus;
        }
        protected virtual ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            throw new NotImplementedException();
        }
    }
}