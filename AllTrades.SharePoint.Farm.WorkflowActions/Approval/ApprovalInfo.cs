﻿using System;
using System.Collections.Generic;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    public class ApprovalInfo {
        public ApprovalInfo() {
            Steps = new List<ApprovalStep>();
            CcUsers = new List<string>();
            Duration = 0;
            DurationUnit = ApprovalDurationUnit.Days;
        }
        public List<ApprovalStep> Steps { get; set; }
        public bool ExpandGroups { get; set; }
        public string Message { get; set; }
        public DateTime DueFate { get; set; }
        public int Duration { get; set; }
        public ApprovalDurationUnit DurationUnit { get; set; }
        public List<string> CcUsers { get; set; }
        public bool EndOnFirstRejection { get; set; }
        public bool EndOnDocumentChange { get; set; }
    }
}