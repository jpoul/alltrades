﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval
{
    public partial class ApprovalProcess
    {
        #region Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCode]
        private void InitializeComponent()
        {
            this.CanModifyActivities = true;
            System.Workflow.Activities.Rules.RuleConditionReference ruleconditionreference1 = new System.Workflow.Activities.Rules.RuleConditionReference();
            System.Workflow.Runtime.CorrelationToken correlationtoken1 = new System.Workflow.Runtime.CorrelationToken();
            System.Workflow.Runtime.CorrelationToken correlationtoken2 = new System.Workflow.Runtime.CorrelationToken();
            this.noApprovalSteps = new System.Workflow.Activities.IfElseBranchActivity();
            this.hasApprovalSteps = new System.Workflow.Activities.IfElseBranchActivity();
            this.sendEmail1 = new Microsoft.SharePoint.WorkflowActions.SendEmail();
            this.checkApprovalSteps = new System.Workflow.Activities.IfElseActivity();
            this.logToHistoryListActivity2 = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.readApprovalSteps = new System.Workflow.Activities.CodeActivity();
            this.logToHistoryListActivity1 = new Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity();
            this.initializeToken = new Microsoft.SharePoint.WorkflowActions.InitializeWorkflow();
            // 
            // noApprovalSteps
            // 
            this.noApprovalSteps.Name = "noApprovalSteps";
            // 
            // hasApprovalSteps
            // 
            ruleconditionreference1.ConditionName = "HasApprovalSteps";
            this.hasApprovalSteps.Condition = ruleconditionreference1;
            this.hasApprovalSteps.Name = "hasApprovalSteps";
            // 
            // sendEmail1
            // 
            this.sendEmail1.BCC = null;
            this.sendEmail1.Body = "Testing";
            this.sendEmail1.CC = null;
            correlationtoken1.Name = "workflowtoken1";
            correlationtoken1.OwnerActivityName = "ApprovalProcess";
            this.sendEmail1.CorrelationToken = correlationtoken1;
            this.sendEmail1.From = null;
            this.sendEmail1.Headers = null;
            this.sendEmail1.IncludeStatus = true;
            this.sendEmail1.Name = "sendEmail1";
            this.sendEmail1.Subject = "Test email";
            this.sendEmail1.To = "jpouliezos@gmail.com";
            // 
            // checkApprovalSteps
            // 
            this.checkApprovalSteps.Activities.Add(this.hasApprovalSteps);
            this.checkApprovalSteps.Activities.Add(this.noApprovalSteps);
            this.checkApprovalSteps.Name = "checkApprovalSteps";
            // 
            // logToHistoryListActivity2
            // 
            this.logToHistoryListActivity2.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logToHistoryListActivity2.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logToHistoryListActivity2.HistoryDescription = "";
            this.logToHistoryListActivity2.HistoryOutcome = "1";
            this.logToHistoryListActivity2.Name = "logToHistoryListActivity2";
            this.logToHistoryListActivity2.OtherData = "";
            this.logToHistoryListActivity2.UserId = -1;
            // 
            // readApprovalSteps
            // 
            this.readApprovalSteps.Name = "readApprovalSteps";
            this.readApprovalSteps.ExecuteCode += new System.EventHandler(this.readApprovalSteps_ExecuteCode);
            // 
            // logToHistoryListActivity1
            // 
            this.logToHistoryListActivity1.Duration = System.TimeSpan.Parse("-10675199.02:48:05.4775808");
            this.logToHistoryListActivity1.EventId = Microsoft.SharePoint.Workflow.SPWorkflowHistoryEventType.WorkflowComment;
            this.logToHistoryListActivity1.HistoryDescription = "";
            this.logToHistoryListActivity1.HistoryOutcome = "1";
            this.logToHistoryListActivity1.Name = "logToHistoryListActivity1";
            this.logToHistoryListActivity1.OtherData = "";
            this.logToHistoryListActivity1.UserId = -1;
            // 
            // initializeToken
            // 
            correlationtoken2.Name = "workflowtoken";
            correlationtoken2.OwnerActivityName = "ApprovalProcess";
            this.initializeToken.CorrelationToken = correlationtoken2;
            this.initializeToken.Name = "initializeToken";
            // 
            // ApprovalProcess
            // 
            this.Activities.Add(this.initializeToken);
            this.Activities.Add(this.logToHistoryListActivity1);
            this.Activities.Add(this.readApprovalSteps);
            this.Activities.Add(this.logToHistoryListActivity2);
            this.Activities.Add(this.checkApprovalSteps);
            this.Activities.Add(this.sendEmail1);
            this.Name = "ApprovalProcess";
            this.CanModifyActivities = false;

        }

        #endregion

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logToHistoryListActivity2;

        private Microsoft.SharePoint.WorkflowActions.LogToHistoryListActivity logToHistoryListActivity1;

        private System.Workflow.Activities.IfElseBranchActivity noApprovalSteps;

        private System.Workflow.Activities.IfElseBranchActivity hasApprovalSteps;

        private System.Workflow.Activities.IfElseActivity checkApprovalSteps;

        private Microsoft.SharePoint.WorkflowActions.InitializeWorkflow initializeToken;

        private System.Workflow.Activities.CodeActivity readApprovalSteps;

        private Microsoft.SharePoint.WorkflowActions.SendEmail sendEmail1;






















    }
}
