﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    public enum ApprovalDurationUnit {
        Undefined = 0,
        Hours,
        Days,
        Weeks,
        Months
    }
}