﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    public class ApprovalStep {
        public string[] Users { get; set; }
        public ApprovalLogic Logic { get; set; }
        public static string SerializeApprovalSteps(List<ApprovalStep> steps) {
            using (var stringWriter = new StringWriter()) {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter)) {
                    var ser = new XmlSerializer(typeof (List<ApprovalStep>));
                    ser.Serialize(xmlWriter, steps);
                }
                return stringWriter.ToString();
            }
        }
        public static List<ApprovalStep> DeserializeApprovalSteps(string serializedObject) {
            using (var stringReader = new StringReader(serializedObject)) {
                using (XmlReader xmlReader = XmlReader.Create(stringReader)) {
                    var serializer = new XmlSerializer(typeof (List<ApprovalStep>));
                    return (List<ApprovalStep>) serializer.Deserialize(xmlReader);
                }
            }
        }
    }
}