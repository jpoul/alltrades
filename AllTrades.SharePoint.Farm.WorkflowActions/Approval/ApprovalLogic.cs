﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    public enum ApprovalLogic {
        Sequence = 0,
        Parallel = 1
    }
}