﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class CreateApprovalStepsToken : AllTradesActivityBase {
        public static DependencyProperty ApprovalStepsTokenProperty = DependencyProperty.Register("ApprovalStepsToken", typeof (string), typeof (CreateApprovalStepsToken));
        public string ApprovalStepsToken {
            get { return (string) (GetValue(ApprovalStepsTokenProperty)); }
            set { SetValue(ApprovalStepsTokenProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            ApprovalStepsToken = ApprovalStep.SerializeApprovalSteps(new List<ApprovalStep>());
            return ActivityExecutionStatus.Closed;
        }
    }
}