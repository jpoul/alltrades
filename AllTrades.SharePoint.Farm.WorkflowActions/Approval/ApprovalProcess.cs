﻿using System;
using System.Collections.Generic;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;
using Microsoft.SharePoint.WorkflowActions;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    public partial class ApprovalProcess : SequenceActivity
    {
        public static readonly DependencyProperty ApprovalStepsTokenProperty = DependencyProperty.Register("ApprovalStepsToken", typeof(string), typeof(ApprovalProcess));
        public static readonly DependencyProperty __ContextProperty = DependencyProperty.Register("__Context", typeof(WorkflowContext), typeof(ApprovalProcess));
        public List<ApprovalStep> ApprovalSteps;
        public int CurrentApprovalStep;
        public ApprovalProcess()
        {
            InitializeComponent();
        }
        public WorkflowContext __Context
        {
            get { return (WorkflowContext)GetValue(__ContextProperty); }
            set { SetValue(__ContextProperty, value); }
        }
        public string ApprovalStepsToken
        {
            get { return (string)(GetValue(ApprovalStepsTokenProperty)); }
            set { SetValue(ApprovalStepsTokenProperty, value); }
        }
        private void readApprovalSteps_ExecuteCode(object sender, EventArgs e)
        {
            ApprovalSteps = ApprovalStep.DeserializeApprovalSteps(ApprovalStepsToken);
        }
    }
}