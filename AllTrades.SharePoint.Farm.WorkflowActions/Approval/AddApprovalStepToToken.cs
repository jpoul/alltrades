﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Approval {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class AddApprovalStepToToken : AllTradesActivityBase {
        public static readonly DependencyProperty ApprovalStepsTokenProperty = DependencyProperty.Register("ApprovalStepsToken", typeof (string), typeof (AddApprovalStepToToken));
        public static readonly DependencyProperty ApproversProperty = DependencyProperty.Register("Approvers", typeof (ArrayList), typeof (AddApprovalStepToToken));
        public static readonly DependencyProperty ApprovalLogicProperty = DependencyProperty.Register("ApprovalLogic", typeof (string), typeof (AddApprovalStepToToken));
        public static DependencyProperty UpdatedApprovalStepsTokenProperty = DependencyProperty.Register("UpdatedApprovalStepsToken", typeof (string), typeof (AddApprovalStepToToken));
        public string ApprovalStepsToken {
            get { return (string) (GetValue(ApprovalStepsTokenProperty)); }
            set { SetValue(ApprovalStepsTokenProperty, value); }
        }
        public string UpdatedApprovalStepsToken {
            get { return (string) (GetValue(UpdatedApprovalStepsTokenProperty)); }
            set { SetValue(UpdatedApprovalStepsTokenProperty, value); }
        }
        public ArrayList Approvers {
            get { return (ArrayList) (GetValue(ApproversProperty)); }
            set { SetValue(ApproversProperty, value); }
        }
        public string ApprovalLogic {
            get { return (string) (GetValue(ApprovalLogicProperty)); }
            set { SetValue(ApprovalLogicProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            List<ApprovalStep> steps = ApprovalStep.DeserializeApprovalSteps(ApprovalStepsToken);
            steps.Add(new ApprovalStep {Logic = (ApprovalLogic) Enum.Parse(typeof (ApprovalLogic), ApprovalLogic), Users = Approvers.ToArray(typeof (string)) as string[]});
            UpdatedApprovalStepsToken = ApprovalStep.SerializeApprovalSteps(steps);
            return ActivityExecutionStatus.Closed;
        }
    }
}