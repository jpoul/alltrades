﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.Constants {
    public static class WorkflowCamlPartTemplates {
        public const string StatusNotStartedTemplate = "<IsNull><FieldRef Name='{0}' /></IsNull>";
        public const string StatusTemplate = "<Eq><FieldRef Name='{0}' /><Value Type='WorkflowStatus'>{1}</Value></Eq>";
    }
}