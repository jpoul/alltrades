﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using AllTrades.SharePoint.Farm.WorkflowActions.Constants;
using AllTrades.SharePoint.Sandbox.Structs;
using AllTrades.SharePoint.Sandbox.Utils;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Utils {
    public static class WorkflowUtils {
        public static Hashtable FoxhoundTaskProperties {
            get {
                return new Hashtable {
                                         {SPBuiltInFieldId.TaskStatus, "In Progress"},
                                         {SPTaskKnownFieldNames.PercentCompleteFieldName, 0.0f},
                                         {SPTaskKnownFieldNames.BodyFieldName, String.Empty}
                                     };
            }
        }
        public static void TriggerWorkflowStartForFolderItem(SPListItem item) {
            if (!SPListUtils.IsFolderItem(item)) throw new ArgumentException("Item is not a folder");
            foreach (SPWorkflowAssociation association in item.ContentType.WorkflowAssociations.Cast<SPWorkflowAssociation>().Where(a => a.AutoStartCreate)) {
                item.ParentList.ParentWeb.Site.WorkflowManager.StartWorkflow(item, association, association.AssociationData);
            }
        }
        public static bool AlterTask(SPListItem task, Hashtable htData, bool fSynchronous, int attempts, int millisecondsTimeout) {
            if ((int) task[SPBuiltInFieldId.WorkflowVersion] != 1) {
                SPList parentList = task.ParentList.ParentWeb.Lists[new Guid(task[SPBuiltInFieldId.WorkflowListId].ToString())];
                SPListItem parentItem = parentList.Items.GetItemById((int) task[SPBuiltInFieldId.WorkflowItemId]);

                for (int i = 0; i < attempts; i++) {
                    SPWorkflow workflow = parentItem.Workflows[new Guid(task[SPBuiltInFieldId.WorkflowInstanceID].ToString())];

                    if (!workflow.IsLocked) {
                        task[SPBuiltInFieldId.WorkflowVersion] = 1;
                        task.SystemUpdate();

                        break;
                    }

                    if (i != attempts - 1)
                        Thread.Sleep(millisecondsTimeout);
                }
            }
            return SPWorkflowTask.AlterTask(task, htData, fSynchronous);
        }
        public static SPListItem GetWorkflowTaskItemBasedOnListItem(SPWorkflowActivationProperties properties, Int32 taskId) {
            return properties.TaskList.GetItemById(taskId);
        }
        public static void GetListItemIdsWithStatus(string webUrl, string listName, string worflowAssociationName, string workflowStatus, uint itemLimit) {
            GetListItemIdsWithStatus(webUrl, listName, worflowAssociationName, null, workflowStatus, itemLimit);
        }
        public static int[] GetListItemIdsWithStatus(string webUrl, string listName, string worflowAssociationName, string contentTypeName, string workflowStatus, uint itemLimit) {
            using (var site = new SPSite(webUrl)) {
                using (SPWeb web = site.OpenWeb()) {
                    SPList list = web.Lists[listName];
                    SPWorkflowAssociation association = GetWorkflowAssociation(list, worflowAssociationName, contentTypeName);
                    if (association == null) {
                        return new int[] {};
                    }
                    string fieldName = list.Fields[worflowAssociationName].StaticName;
                    var sb = new StringBuilder();
                    sb.Append("<Where>");
                    if (!string.IsNullOrEmpty(contentTypeName)) {
                        sb.Append("<And>");
                        sb.Append(string.Format(CommonCamlPartTemplates.CamlContentTypeTemplate, contentTypeName));
                    }
                    if (workflowStatus == "N") {
                        sb.Append(string.Format(WorkflowCamlPartTemplates.StatusNotStartedTemplate, fieldName));
                    } else {
                        sb.Append(string.Format(WorkflowCamlPartTemplates.StatusTemplate, fieldName, workflowStatus));
                    }
                    if (!string.IsNullOrEmpty(contentTypeName)) {
                        sb.Append("</And>");
                    }
                    sb.Append("</Where>");
                    var oQuery = new SPQuery {
                                                 Query = sb.ToString(),
                                                 RowLimit = itemLimit,
                                                 ViewAttributes = "Scope=\"RecursiveAll\""
                                             };
                    return list.GetItems(oQuery).Cast<SPListItem>().Select(itm => itm.ID).ToArray();
                }
            }
        }
        public static void RestartListItemWorkflow(SPSite site, SPListItem item, SPWorkflowAssociation association) {
            SPWorkflow workflow = site.WorkflowManager.GetItemActiveWorkflows(item).Cast<SPWorkflow>().Where(wf => wf.AssociationId == association.Id).SingleOrDefault();
            if (workflow == null) return;
            //if ((workflow.InternalState.ToString() != "Running, Faulting, Terminated")) return;
            SPWorkflowManager.CancelWorkflow(workflow);
            site.WorkflowManager.StartWorkflow(item, association, association.AssociationData, true);
        }
        public static SPWorkflowAssociation GetWorkflowAssociation(SPList list, string workflowAssociationName) {
            return GetWorkflowAssociation(list, workflowAssociationName, null);
        }
        public static SPWorkflowAssociation GetWorkflowAssociation(SPList list, string workflowAssociationName, string contentTypeName) {
            if (String.IsNullOrEmpty(contentTypeName) || contentTypeName == "Any") {
                return list.WorkflowAssociations.GetAssociationByName(workflowAssociationName, Thread.CurrentThread.CurrentCulture);
            }
            SPContentType contentType = SPListUtils.GetContentTypeOfListByName(list, contentTypeName);
            if (contentType == null) {
                throw new ArgumentException("The specified content type was not found in the list.");
            }
            return contentType.WorkflowAssociations.GetAssociationByName(workflowAssociationName, Thread.CurrentThread.CurrentCulture);
        }
    }
}