﻿using System;
using System.Workflow.ComponentModel;
using Microsoft.SharePoint.WorkflowActions;

namespace AllTrades.SharePoint.Farm.WorkflowActions.Utils {
    public static class ActivityUtils {
        /// <summary>
        /// resolves all workflow lookup items within a string
        /// </summary>
        /// <param name="context"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ProcessStringField(ActivityExecutionContext context, string str) {
            if (String.IsNullOrEmpty(str))
                return String.Empty;

            Activity parent = context.Activity;

            while (parent.Parent != null) {
                parent = parent.Parent;
            }

            return Helper.ProcessStringField(str, parent, null);
        }
    }
}