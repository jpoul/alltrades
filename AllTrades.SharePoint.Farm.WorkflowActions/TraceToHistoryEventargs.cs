﻿using System;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    public class TraceToHistoryEventargs : EventArgs {
        private readonly TraceToHistoryPoint _tracePoint;
        public TraceToHistoryEventargs(TraceToHistoryPoint tracePoint) {
            _tracePoint = tracePoint;
        }
        public TraceToHistoryEventargs(TraceToHistoryPoint tracePoint, Exception exception) {
            _tracePoint = tracePoint;
            Exception = exception;
        }
        public TraceToHistoryPoint TracePoint {
            get { return _tracePoint; }
        }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }
}