﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ODC {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class AddODCParametersToToken : AllTradesActivityBase {
        public static readonly DependencyProperty ODCParametersTokenProperty = DependencyProperty.Register("ODCParametersToken", typeof (string), typeof (AddODCParametersToToken));
        public static readonly DependencyProperty ODCParameterNameProperty = DependencyProperty.Register("ODCParameterName", typeof (string), typeof (AddODCParametersToToken));
        public static readonly DependencyProperty ODCParameterValueProperty = DependencyProperty.Register("ODCParameterValue", typeof (string), typeof (AddODCParametersToToken));
        public static readonly DependencyProperty ODCNameProperty = DependencyProperty.Register("ODCName", typeof (string), typeof (AddODCParametersToToken));
        public static readonly DependencyProperty ODCTokenTypeProperty = DependencyProperty.Register("ODCTokenType", typeof (string), typeof (AddODCParametersToToken));
        public static DependencyProperty UpdatedODCParametersTokenProperty = DependencyProperty.Register("UpdatedODCParametersToken", typeof (string), typeof (AddODCParametersToToken));
        public string ODCParametersToken {
            get { return (string) (GetValue(ODCParametersTokenProperty)); }
            set { SetValue(ODCParametersTokenProperty, value); }
        }
        public string UpdatedODCParametersToken {
            get { return (string)(GetValue(UpdatedODCParametersTokenProperty)); }
            set { SetValue(UpdatedODCParametersTokenProperty, value); }
        }
        public string ODCParameterName {
            get { return (string) (GetValue(ODCParameterNameProperty)); }
            set { SetValue(ODCParameterNameProperty, value); }
        }
        public string ODCParameterValue {
            get { return (string) (GetValue(ODCParameterValueProperty)); }
            set { SetValue(ODCParameterValueProperty, value); }
        }
        public string ODCName {
            get { return (string) (GetValue(ODCNameProperty)); }
            set { SetValue(ODCNameProperty, value); }
        }
        public string ODCTokenType {
            get { return (string) (GetValue(ODCTokenTypeProperty)); }
            set { SetValue(ODCTokenTypeProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            List<ODCParameter> steps = ODCParameter.DeserializeODCParameters(ODCParametersToken);
            steps.Add(new ODCParameter {ParameterName = ODCParameterName, ParameterValue = ODCParameterValue, Name = ODCName, TokenType = (ODCTokenType) Enum.Parse(typeof (ODCTokenType), ODCTokenType)});
            UpdatedODCParametersToken = ODCParameter.SerializeODCParameters(steps);
            return ActivityExecutionStatus.Closed;
        }
    }
}