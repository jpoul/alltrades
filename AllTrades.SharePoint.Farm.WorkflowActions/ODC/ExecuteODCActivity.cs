﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.IO;
using System.Linq;
using System.Workflow.ComponentModel;
using AllTrades.Files;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ODC {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class ExecuteODCActivity : AllTradesActivityBase {
        public static readonly DependencyProperty ODCParametersTokenProperty = DependencyProperty.Register("ODCParametersToken", typeof(string), typeof(ExecuteODCActivity));
        public static DependencyProperty ResultProperty = DependencyProperty.Register("Result", typeof(string), typeof(ExecuteODCActivity));
        public string ODCParametersToken
        {
            get { return (string)(GetValue(ODCParametersTokenProperty)); }
            set { SetValue(ODCParametersTokenProperty, value); }
        }
        public string Result
        {
            get { return (string) GetValue(ResultProperty); }
            set { SetValue(ResultProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.OpenWeb(__Context.Web.ID)) {
                    if (ListItem == -1) {
                        throw new ArgumentException("Connection file not found.");
                    }

                    var docLib = (SPDocumentLibrary) (web.Lists[new Guid(ListId)]);
                    SPListItem item = docLib.GetItemById(ListItem);
                    using (Stream ms = new MemoryStream(item.File.OpenBinary())) {
                        var doc = new ODCConnectionFile(ms);

                        var parameters = ODCParameter.DeserializeODCParameters(ODCParametersToken).Where(odc=>odc.Name == System.IO.Path.GetFileNameWithoutExtension(item.File.Name));
                        string[] pars = (parameters.Where(odcParameter => odcParameter.TokenType == ODCTokenType.AddParameter)
                            .Select(odcParameter => odcParameter.ParameterValue)).ToArray();
                        string[] cmdTokens = (parameters.Where(odcParameter => odcParameter.TokenType == ODCTokenType.ReplaceCommandText)
                            .Select(odcParameter => odcParameter.ParameterName)).ToArray();
                        string[] cmdTokenValues = (parameters.Where(odcParameter => odcParameter.TokenType == ODCTokenType.ReplaceCommandText)
                            .Select(odcParameter => odcParameter.ParameterValue)).ToArray();
                       DataSet ds = doc.Execute(pars, cmdTokens, cmdTokenValues);
                        using (var sw = new StringWriter()) {
                            ds.WriteXml(sw);
                            Result = sw.ToString();
                            sw.Close();
                        }
                    }
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}