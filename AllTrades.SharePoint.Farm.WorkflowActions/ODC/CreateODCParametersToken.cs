﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ODC {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class CreateODCParametersToken : AllTradesActivityBase {
        public static DependencyProperty ODCParametersTokenProperty = DependencyProperty.Register("ODCParametersToken", typeof(string), typeof(CreateODCParametersToken));
        public string ODCParametersToken {
            get { return (string) (GetValue(ODCParametersTokenProperty)); }
            set { SetValue(ODCParametersTokenProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            ODCParametersToken = ODCParameter.SerializeODCParameters(new List<ODCParameter>());
            return ActivityExecutionStatus.Closed;
        }
    }
}