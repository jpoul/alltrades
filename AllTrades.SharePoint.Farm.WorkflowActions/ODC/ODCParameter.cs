﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ODC {
    public class ODCParameter {
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public string Name { get; set; }
        public ODCTokenType TokenType { get; set; }
        public static string SerializeODCParameters(List<ODCParameter> steps) {
            using (var stringWriter = new StringWriter()) {
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter)) {
                    var ser = new XmlSerializer(typeof (List<ODCParameter>));
                    ser.Serialize(xmlWriter, steps);
                }
                return stringWriter.ToString();
            }
        }
        public static List<ODCParameter> DeserializeODCParameters(string serializedObject) {
            using (var stringReader = new StringReader(serializedObject)) {
                using (XmlReader xmlReader = XmlReader.Create(stringReader)) {
                    var serializer = new XmlSerializer(typeof (List<ODCParameter>));
                    return (List<ODCParameter>) serializer.Deserialize(xmlReader);
                }
            }
        }
    }
}