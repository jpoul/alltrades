﻿namespace AllTrades.SharePoint.Farm.WorkflowActions.ODC {
    public enum ODCTokenType {
        AddParameter = 0,
        ReplaceCommandText = 1
    }
}