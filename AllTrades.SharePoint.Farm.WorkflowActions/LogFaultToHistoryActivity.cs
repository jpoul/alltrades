﻿using System;
using System.Workflow.Activities;
using System.Workflow.ComponentModel;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    public partial class LogFaultToHistoryActivity : SequenceActivity {
        public static readonly DependencyProperty FaultProperty = DependencyProperty.Register("Fault", typeof (Exception), typeof (LogFaultToHistoryActivity));
        public static readonly DependencyProperty FaultTypeProperty = DependencyProperty.Register("FaultType", typeof (Type), typeof (LogFaultToHistoryActivity));
        public LogFaultToHistoryActivity() {
            InitializeComponent();
        }
        public Type FaultType {
            get { return (Type) GetValue(FaultTypeProperty); }
            set { SetValue(FaultTypeProperty, value); }
        }
        public Exception Fault {
            get { return (Exception) GetValue(FaultProperty); }
            set { SetValue(FaultProperty, value); }
        }
    }
}