using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using AllTrades.SharePoint.Sandbox.Utils;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class CreateContainerActivity : AllTradesActivityBase {
        public static readonly DependencyProperty ContentTypeNameProperty = DependencyProperty.Register("ContentTypeName", typeof (string), typeof (CreateContainerActivity));
        public static readonly DependencyProperty ContainerNameProperty = DependencyProperty.Register("ContainerName", typeof (object), typeof (CreateContainerActivity));
        public static DependencyProperty NewItemIdProperty = DependencyProperty.Register("NewItemId", typeof (int), typeof (CreateContainerActivity));
        public static readonly DependencyProperty ContainerTypeProperty = DependencyProperty.Register("ContainerType", typeof (string), typeof (CreateContainerActivity));
        public static readonly DependencyProperty ReturnExistingProperty = DependencyProperty.Register("ReturnExisting", typeof (string), typeof (CreateContainerActivity));
        public string ContainerType {
            get { return (string) GetValue(ContainerTypeProperty); }
            set { SetValue(ContainerTypeProperty, value); }
        }
        public int NewItemId {
            get { return (int) GetValue(NewItemIdProperty); }
            set { SetValue(NewItemIdProperty, value); }
        }
        public string ContentTypeName {
            get { return (string) GetValue(ContentTypeNameProperty); }
            set { SetValue(ContentTypeNameProperty, value); }
        }
        public object ContainerName {
            get { return GetValue(ContainerNameProperty); }
            set { SetValue(ContainerNameProperty, value); }
        }
        public string ReturnExisting {
            get { return (string) GetValue(ReturnExistingProperty); }
            set { SetValue(ReturnExistingProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.AllWebs[__Context.Web.ID]) {
                    SPList list = web.Lists[new Guid(ListId)];
                    var query = new SPQuery {Query = string.Format("<Where><Eq><FieldRef Name='Title'/><Value Type='Text'>{0}</Value></Eq></Where>", ContainerName)};
                    SPListItemCollection spListItemCollection = list.GetItems(query);
                    if (spListItemCollection.Count > 0) {
                        if (Convert.ToBoolean(ReturnExisting)) {
                            NewItemId = spListItemCollection[0].ID;
                            return ActivityExecutionStatus.Closed;
                        }
                        throw new ArgumentException("Container with that name already exists.");
                    }
                    SPContentType contentType = SPListUtils.GetContentTypeOfListByName(list, ContentTypeName);
                    if (!contentType.Id.IsChildOf(SPBuiltInContentTypeId.Folder)) {
                        throw new ArgumentException("Content type is not a container object");
                    }
                    NewItemId = CreateContainer(list, contentType);
                }
            }
            return ActivityExecutionStatus.Closed;
        }
        protected virtual int CreateContainer(SPList list, SPContentType contentType) {
            return SPListUtils.CreateFolder(list.RootFolder, (string) ContainerName, contentType.Id);
        }
    }
}