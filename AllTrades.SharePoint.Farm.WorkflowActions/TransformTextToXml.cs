using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.IO;
using System.Text;
using System.Workflow.ComponentModel;
using System.Xml;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using AllTrades.Utils;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class TransformTextToXml : AllTradesActivityBase {
        public static readonly DependencyProperty XslDocumentLibraryIdProperty = DependencyProperty.Register("XslDocumentLibraryId", typeof (string), typeof (TransformTextToXml));
        public static readonly DependencyProperty XslDocumentIdProperty = DependencyProperty.Register("XslDocumentId", typeof (int), typeof (TransformTextToXml));
        public static readonly DependencyProperty XmlDocumentLibraryIdProperty = DependencyProperty.Register("XmlDocumentLibraryId", typeof (string), typeof (TransformTextToXml));
        public static readonly DependencyProperty XmlDocumentIdProperty = DependencyProperty.Register("XmlDocumentId", typeof (int), typeof (TransformTextToXml));
        public static readonly DependencyProperty DocumentEncodingProperty = DependencyProperty.Register("DocumentEncoding", typeof (string), typeof (TransformTextToXml));
        public static readonly DependencyProperty ResultFileNameProperty = DependencyProperty.Register("ResultFileName", typeof (string), typeof (TransformTextToXml));
        public string DocumentEncoding {
            get { return (string) GetValue(DocumentEncodingProperty); }
            set { SetValue(DocumentEncodingProperty, value); }
        }
        public string ResultFileName {
            get { return (string) GetValue(ResultFileNameProperty); }
            set { SetValue(ResultFileNameProperty, value); }
        }
        public string XslDocumentLibraryId {
            get { return (string) GetValue(XslDocumentLibraryIdProperty); }
            set { SetValue(XslDocumentLibraryIdProperty, value); }
        }
        public int XslDocumentId {
            get { return (int) GetValue(XslDocumentIdProperty); }
            set { SetValue(XslDocumentIdProperty, value); }
        }
        public string XmlDocumentLibraryId {
            get { return (string) GetValue(XmlDocumentLibraryIdProperty); }
            set { SetValue(XmlDocumentLibraryIdProperty, value); }
        }
        public int XmlDocumentId {
            get { return (int) GetValue(XmlDocumentIdProperty); }
            set { SetValue(XmlDocumentIdProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.AllWebs[__Context.Web.ID]) {
                    var xlsDocumentLibraryId = new Guid(XslDocumentLibraryId);
                    SPList xlsDocumentLibrary = web.Lists[xlsDocumentLibraryId];
                    SPListItem xlsDocumentItem = xlsDocumentLibrary.Items.GetItemById(XslDocumentId);
                    Stream xslFileStream = xlsDocumentItem.File.OpenBinaryStream();

                    var currentDocumentListId = new Guid(ListId);
                    SPList currentList = web.Lists[currentDocumentListId];
                    SPListItem currentItem = currentList.Items.GetItemById(ListItem);
                    Stream currentDocumentStream = currentItem.File.OpenBinaryStream();

                    XmlDocument xmlResult = XslUtils.TransformTextToXml(currentDocumentStream, Encoding.GetEncoding(DocumentEncoding), xslFileStream);

                    var resultContainerDocumentListId = new Guid(XmlDocumentLibraryId);
                    SPList resultContainerDocumentList = web.Lists[resultContainerDocumentListId];
                    SPListItem resultContainerItem = resultContainerDocumentList.Items.GetItemById(XmlDocumentId);
                    var resultStream = new MemoryStream();
                    xmlResult.Save(resultStream);
                    resultContainerItem.Folder.Files.Add(string.Format("{0}.xml", ResultFileName), resultStream, true);
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}