using System.Workflow.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design {
    [ActivityDesignerTheme(typeof (AllTradesActivitiesTheme))]
    public class AllTradesActivityDesigner : ActivityDesigner {}
}