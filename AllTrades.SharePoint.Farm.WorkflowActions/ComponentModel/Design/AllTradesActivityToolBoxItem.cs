using System;
using System.Runtime.Serialization;
using System.Workflow.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design {
    [Serializable]
    public class AllTradesActivityToolBoxItem : ActivityToolboxItem {
        public AllTradesActivityToolBoxItem(Type type)
            : base(type) {}
        private AllTradesActivityToolBoxItem(SerializationInfo info, StreamingContext context) {
            base.Deserialize(info, context);
        }
    }
}