using System.Drawing;
using System.Drawing.Drawing2D;
using System.Workflow.ComponentModel.Design;

namespace AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design {
    public sealed class AllTradesActivitiesTheme : ActivityDesignerTheme {
        public AllTradesActivitiesTheme(WorkflowTheme theme)
            : base(theme) {
            ForeColor = Color.FromArgb(0xff, 0, 0, 0);
            BorderColor = Color.FromArgb(0xff, 0x94, 0xb6, 0xf7);
            BorderStyle = DashStyle.Solid;
            BackColorStart = Color.FromKnownColor(KnownColor.WhiteSmoke);
            BackColorEnd = Color.FromKnownColor(KnownColor.AliceBlue);
            BackgroundStyle = LinearGradientMode.Horizontal;
        }
    }
}