using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.Utils;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using Microsoft.BusinessData.Runtime;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class SetBusinessEntityReference : AllTradesActivityBase {
        public static readonly DependencyProperty EntityReferenceIdProperty = DependencyProperty.Register("EntityReferenceId", typeof (string), typeof (SetBusinessEntityReference));
        public static readonly DependencyProperty FieldNameProperty = DependencyProperty.Register("FieldName", typeof (string), typeof (SetBusinessEntityReference));
        public static readonly DependencyProperty NameSpaceProperty = DependencyProperty.Register("NameSpace", typeof (string), typeof (SetBusinessEntityReference));
        public static readonly DependencyProperty EntityNameProperty = DependencyProperty.Register("EntityName", typeof (string), typeof (SetBusinessEntityReference));
        public string EntityReferenceId {
            get { return (string) GetValue(EntityReferenceIdProperty); }
            set { SetValue(EntityReferenceIdProperty, value); }
        }
        public string FieldName {
            get { return (string) GetValue(FieldNameProperty); }
            set { SetValue(FieldNameProperty, value); }
        }
        public string NameSpace {
            get { return (string) GetValue(NameSpaceProperty); }
            set { SetValue(NameSpaceProperty, value); }
        }
        public string EntityName {
            get { return (string) GetValue(EntityNameProperty); }
            set { SetValue(EntityNameProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            IEntityInstance entityInstance = BusinessDataUtils.FindEntityInstanceById(__Context.Site.Url, NameSpace, EntityName, EntityReferenceId);
            if (entityInstance == null) {
                throw new ArgumentException("Business entity not found.");
            }
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.AllWebs[__Context.Web.ID]) {
                    var listId = new Guid(ListId);
                    SPList list = web.Lists[listId];
                    SPListItem item = list.Items.GetItemById(ListItem);
                    BusinessDataUtils.SetBusinessFieldValue(FieldName, item, entityInstance);
                    item.Update();
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}