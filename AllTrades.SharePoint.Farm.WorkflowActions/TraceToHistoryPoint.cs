﻿namespace AllTrades.SharePoint.Farm.WorkflowActions {
    public enum TraceToHistoryPoint {
        Start = 0,
        Finish,
        Fault,
        Information
    }
}