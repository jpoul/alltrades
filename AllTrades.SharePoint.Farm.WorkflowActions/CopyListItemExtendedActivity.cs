using System;
using System.ComponentModel;
using System.Workflow.ComponentModel;
using System.Workflow.ComponentModel.Compiler;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Compiler;
using AllTrades.SharePoint.Farm.WorkflowActions.Utils;
using AllTrades.SharePoint.Sandbox;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [ActivityValidator(typeof (CopyListItemActivityValidator))]
    public class CopyListItemExtendedActivity : AllTradesActivityBase {
        public static DependencyProperty MoveProperty = DependencyProperty.Register("Move", typeof (string), typeof (CopyListItemExtendedActivity));
        public static DependencyProperty OverwriteProperty = DependencyProperty.Register("Overwrite", typeof (string), typeof (CopyListItemExtendedActivity));
        public static DependencyProperty DestinationListUrlProperty = DependencyProperty.Register("DestinationListUrl", typeof (string), typeof (CopyListItemExtendedActivity));
        public static DependencyProperty OutListItemIdProperty = DependencyProperty.Register("OutListItemId", typeof (int), typeof (CopyListItemExtendedActivity));
        [Description("Specifies whether to move an item or simply copy it")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [ValidationOption(ValidationOption.Required)]
        public string Move {
            get { return ((string) (GetValue(MoveProperty))); }
            set { SetValue(MoveProperty, value); }
        }
        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [ValidationOption(ValidationOption.Required)]
        public string Overwrite {
            get { return ((string) (GetValue(OverwriteProperty))); }
            set { SetValue(OverwriteProperty, value); }
        }
        [Description("Destination list url")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [ValidationOption(ValidationOption.Required)]
        public string DestinationListUrl {
            get { return ((string) (GetValue(DestinationListUrlProperty))); }
            set { SetValue(DestinationListUrlProperty, value); }
        }
        [Description("This is the description which appears in the Property Browser")]
        [Category("This is the category which will be displayed in the Property Browser")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int OutListItemId {
            get { return ((int) (GetValue(OutListItemIdProperty))); }
            set { SetValue(OutListItemIdProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var sourceSite = new SPSite(__Context.Web.Site.ID)) {
                using (SPWeb sourceWeb = sourceSite.AllWebs[__Context.Web.ID]) {
                    //replace any workflow variables
                    string destinationUrlProcessed = ActivityUtils.ProcessStringField(executionContext, DestinationListUrl);
                    using (var destSite = new SPSite(destinationUrlProcessed)) {
                        using (SPWeb destWeb = destSite.OpenWeb()) {
                            SPFolder destFolder = destWeb.GetFolder(destinationUrlProcessed);
                            if (!destFolder.Exists)
                                throw new InvalidOperationException(string.Format("List at {0} does not exist!", DestinationListUrl));
                            SPList destinationList = destWeb.Lists[destFolder.ParentListId];
                            SPList sourceList = sourceWeb.Lists[new Guid(ListId)];
                            SPListItem sourceItem = sourceList.Items.GetItemById(ListItem);
                            var options = new ListItemCopier.ListItemCopyOptions {IncludeAttachments = true, OperationType = bool.Parse(Move) ? ListItemCopier.OperationType.Move : ListItemCopier.OperationType.Copy, Overwrite = bool.Parse(Overwrite), DestinationFolder = destFolder};
                            using (var myCopier = new ListItemCopier(sourceItem, destinationList, options)) {
                                OutListItemId = myCopier.Copy();
                            }
                        }
                    }
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}