﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using AllTrades.SharePoint.Sandbox.Utils;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [ToolboxBitmap(typeof (UpdateItemContentTypeActivity), "Resources.Activity.png")]
    [Designer(typeof (AllTradesActivityDesigner), typeof (IDesigner))]
    [ToolboxItem(typeof (AllTradesActivityToolBoxItem))]
    public class UpdateItemContentTypeActivity : AllTradesActivityBase {
        public static readonly DependencyProperty ContentTypeNameProperty = DependencyProperty.Register("ContentTypeName", typeof (string), typeof (UpdateItemContentTypeActivity));
        public string ContentTypeName {
            get { return (string) GetValue(ContentTypeNameProperty); }
            set { SetValue(ContentTypeNameProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            using (var site = new SPSite(__Context.Site.ID)) {
                using (SPWeb web = site.AllWebs[__Context.Web.ID]) {
                    var listId = new Guid(ListId);
                    SPList list = web.Lists[listId];
                    SPListItem item = list.Items.GetItemById(ListItem);
                    SPContentType contentType = SPListUtils.GetContentTypeOfListByName(list, ContentTypeName);
                    SPListUtils.UpdateItemContentType(item, contentType.Id);
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}