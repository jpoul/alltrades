﻿using System;
using System.Runtime.Serialization;

namespace AllTrades.SharePoint.Farm.WorkflowActions {
    [Serializable]
    public class AllTradesActivityException : Exception {
        public AllTradesActivityException() {}
        public AllTradesActivityException(string message) : base(message) {}
        public AllTradesActivityException(string message, Exception inner) : base(message, inner) {}
        protected AllTradesActivityException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) {}
    }
}