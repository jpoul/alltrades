﻿using System;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client {
    public class Utils {
        public static Guid Sp2010EnterpriseSiteFeatureId = new Guid("0806D127-06E6-447a-980E-2E90B03101B8");
        public static Guid Sp2010DataConnectionLibraryFeatureId = new Guid("00BFEA71-DBD7-4F72-B8CB-DA7AC0440130");

        public static ListCreationInformation CreateDataConnectionLibraryCreationInfo(string title, string url, string description, QuickLaunchOptions showOnQuickLaunch) {
            return new ListCreationInformation {
                    Title = title,
                    Url = url,
                    Description = description,
                    QuickLaunchOption = showOnQuickLaunch,
                    TemplateFeatureId = Sp2010DataConnectionLibraryFeatureId,
                    TemplateType = 130,
                    DocumentTemplateType = 130
                };
        }
    }
}
