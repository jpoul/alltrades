﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client {
    /// <summary>
    /// This example shows how to retrieve list item data 
    /// from an external list.
    /// 
    /// You'll need to explicitly specify the field data in both
    /// the CAML query and also ClientContext.Load.
    /// </summary>
    public class SPClientQuery {
        private readonly string _targetListName;
        private readonly string _targetSiteUrl;

        public SPClientQuery(string targetSiteUrl, string targetListName) {
            _targetListName = targetListName;
            _targetSiteUrl = targetSiteUrl;
        }

        /// <summary> 
        /// Example to show using CSOM to retrieve external List data.        
        /// </summary>        
        public ListItem[] Execute(string viewName = null, Hashtable filters = null, string camlQuery = "<Query/>" ) {
            var clientContext = new ClientContext(_targetSiteUrl);
            var externalList = clientContext.Web.Lists.GetByTitle(_targetListName);

            // To properly construct the CamlQuery and 
            // ClientContext.LoadQuery,
            // we need some View data of the Virtual List.
            // In particular, the View will give us the CamlQuery 
            // Method and Fields.
            clientContext.Load(
                externalList.Views,
                viewCollection => viewCollection.Include(
                    view => view.Title,
                    view => view.ViewFields,
                    view => view.HtmlSchemaXml));

            // This tells us how many list items we can retrieve.
            clientContext.Load(clientContext.Site, s => s.MaxItemsPerThrottledOperation);

            clientContext.ExecuteQuery();
            var allViews = externalList.Views.ToList();
            var targetView = string.IsNullOrEmpty(viewName) ? allViews[0] : allViews.FirstOrDefault(view => view.Title == viewName);
            if (targetView == null) {
                throw new ArgumentException("View not found");
            }
            var method = ReadMethodFromViewXml(targetView.HtmlSchemaXml);
            var viewFields = targetView.ViewFields;
            
            var vlQuery = CreateCamlQuery(
                clientContext.Site.MaxItemsPerThrottledOperation,
                method,
                viewFields, 
                filters,
                camlQuery);
            
            var listItemExpressions = CreateListItemLoadExpressions(viewFields);
            var listItemCollection = externalList.GetItems(vlQuery);
            var resultData = clientContext.LoadQuery(listItemCollection.Include(listItemExpressions));
            clientContext.ExecuteQuery();
            return resultData.ToArray();
        }

        /// <summary>
        /// Parses the viewXml and returns the Method value.
        /// </summary>        
        private static string ReadMethodFromViewXml(string viewXml) {
            var readerSettings = new XmlReaderSettings {ConformanceLevel = ConformanceLevel.Fragment};

            var xmlReader = XmlReader.Create(new StringReader(viewXml), readerSettings);
            while (xmlReader.Read()) {
                switch (xmlReader.NodeType) {
                    case XmlNodeType.Element:
                        if (xmlReader.Name == "Method") {
                            while (xmlReader.MoveToNextAttribute()) {
                                if (xmlReader.Name == "Name") {
                                    return xmlReader.Value;
                                }
                            }
                        }
                        break;
                }
            }
            return string.Empty;
        }

        private static CamlQuery CreateCamlQuery(uint rowLimit, string method, ViewFieldCollection viewFields, Hashtable filters = null, string camlQuery = "<Query/>")
        {
            var query = new CamlQuery();

            var xmlSettings = new XmlWriterSettings {OmitXmlDeclaration = true};

            var stringBuilder = new StringBuilder();
            var writer = XmlWriter.Create(stringBuilder, xmlSettings);

            writer.WriteStartElement("View");

            // Specifies we want all items, regardless of folder level.
            writer.WriteAttributeString("Scope", "RecursiveAll");

            if (!string.IsNullOrEmpty(method)) {
                writer.WriteStartElement("Method");
                writer.WriteAttributeString("Name", method);
                if (filters != null) {
                    foreach (DictionaryEntry filter in filters) {
                        writer.WriteStartElement("Filter");
                        writer.WriteAttributeString("Name", filter.Key.ToString());
                        writer.WriteAttributeString("Value", filter.Value.ToString());
                        writer.WriteEndElement();
                    }
                }
                writer.WriteEndElement(); // Method
            }

            if (viewFields.Count > 0) {
                writer.WriteStartElement("ViewFields");
                foreach (var viewField in viewFields) {
                    writer.WriteStartElement("FieldRef");
                    writer.WriteAttributeString("Name", viewField);
                    writer.WriteEndElement(); // FieldRef
                }
                writer.WriteEndElement(); // ViewFields
            }

            writer.WriteRaw(camlQuery);

            writer.WriteElementString(
                "RowLimit", rowLimit.ToString(CultureInfo.InvariantCulture));

            writer.WriteEndElement(); // View

            writer.Close();

            query.ViewXml = stringBuilder.ToString();

            return query;
        }

        /// <summary>
        /// Returns an array of Expression used in 
        /// ClientContext.LoadQuery to retrieve 
        /// the specified field data from a ListItem.        
        /// </summary>        
        private static Expression<Func<ListItem, object>>[] CreateListItemLoadExpressions(IEnumerable<string> viewFields, bool isExternal = false) {
            var expressions = viewFields.Select(fieldInternalName => (Expression<Func<ListItem, object>>) (listItem => listItem[fieldInternalName])).ToList();
            var idFieldName = isExternal ? "BdcIdentity" : "Id";
            expressions.Add(listItem => listItem[idFieldName]);
            return expressions.ToArray();
        }
    }
}