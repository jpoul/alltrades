﻿using System;
using System.Collections.Generic;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client.ExternalData {
    public static class Extensions {       
        public static IDictionary<String, String> InitializeBcsDictionary(this ListDataSource listDataSource, String entity, String entityNamespace, String lobSystemInstance, String specificFinder) {
            return new Dictionary<String, String> {{"Entity", entity}, {"EntityNamespace", entityNamespace}, {"LobSystemInstance", lobSystemInstance}, {"SpecificFinder", specificFinder}};
        }
    }
}
