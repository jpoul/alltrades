﻿using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client.ExternalData {
    public static class Utils {
        public static ListCreationInformation CreateBcsListCreationInformation(string title, string url, string description, QuickLaunchOptions showOnQuickLaunch, string entityName, string entityNamespace, string lobSystemInstance, string specificFinderName) {
            var listDataSource = new ListDataSource();
            var dictionary = listDataSource.InitializeBcsDictionary(entityName, entityNamespace, lobSystemInstance, specificFinderName);
            return new ListCreationInformation {
                Title = title,
                Url = url.StartsWith("Lists/") ? url : string.Format("Lists/{0}", url),
                Description = description,
                QuickLaunchOption = showOnQuickLaunch,
                DataSourceProperties = dictionary
            };
        }

        public static ListCreationInformation CreateBcsListCreationInformation(string entityName, string entityNamespace, string lobSystemInstance, string specificFinderName) {
            return CreateBcsListCreationInformation(entityName, string.Format("Lists/{0}", entityName), "", QuickLaunchOptions.DefaultValue, entityName, entityNamespace, lobSystemInstance, specificFinderName);
        }
    }
}