﻿using System;
using System.Collections.Generic;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client {
    public static class Extensions {
        public static bool IsDocumentLibrary(this List list) {
            return list.BaseType == BaseType.DocumentLibrary;
        }
    }
}
