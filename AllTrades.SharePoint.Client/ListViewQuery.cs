﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Xml.Linq;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client {
    public class ListViewQuery
    {
        public string ViewXml { get; set; }
        public Expression<Func<ListItemCollection, object>> Fields { get; set; }
        public string AbsoluteWebUrl { get; set; }
        public string[] Criteria { get; set;}
        public string ListName { get; set; }
        public ICredentials Credentials { get; set; }

        public IEnumerable<XElement> GetFieldsOfView() {
            return GetFieldsOfView(ViewXml);
        }
        public ListItemCollection ExecuteQuery() {
            string query = string.IsNullOrEmpty(ViewXml) ? "" : string.Format(ViewXml, Criteria);
            return ExecuteQuery(AbsoluteWebUrl, ListName, query, Fields, Credentials);
        }
        public static IEnumerable<XElement> GetFieldsOfView(string viewXml)
        {
            if (string.IsNullOrEmpty(viewXml)) { return null; }
            try
            {
                return XDocument.Parse(viewXml).Descendants("FieldRef");
            }
            catch
            {
                return null;
            }
        }
        public static ListItemCollection ExecuteQuery(string webUrl, string listName, string viewXml, Expression<Func<ListItemCollection, object>> fieldsExpression, ICredentials credentials) {
            var context = new ClientContext(webUrl);
            var site = context.Web;
            var list = site.Lists.GetByTitle(listName);
            var camlQuery = new CamlQuery {ViewXml = viewXml};
            ListItemCollection listItems = list.GetItems(camlQuery);
            context.Credentials = credentials;
            context.AuthenticationMode = ClientAuthenticationMode.Default;
            if (fieldsExpression != null)
            {
                context.Load(listItems, fieldsExpression);
            }
            else {
                context.Load(listItems);
            }
            context.ExecuteQuery();
            return listItems;
        }

        public void AddListItem(string[] fields, List<string[]> values)  {
            var context = new ClientContext(AbsoluteWebUrl);
            var site = context.Web;
            var list = site.Lists.GetByTitle(ListName);
            var info = new ListItemCreationInformation();
            foreach (var itemValues in values) {
                var li = list.AddItem(info);
                for (int index = 0; index < fields.Length; index++) {
                    var field = fields[index];
                    var value = itemValues[index];
                    li[field] = value;
                }
                li.Update();
            }
            context.ExecuteQuery();
        }
    }
}