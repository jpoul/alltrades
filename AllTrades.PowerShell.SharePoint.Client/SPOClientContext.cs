﻿using Microsoft.SharePoint.Client;

namespace AllTrades.PowerShell.SharePoint.Client {
    public class PSClientContext : ClientContext {
        public PSClientContext(string siteUrl): base(siteUrl) {
            // this method gives a popup asking for credentials
            //CookieCollection cookies = ClaimClientContext.GetAuthenticatedCookies(siteUrl, 0, 0);

            //this.ExecutingWebRequest += delegate(object sender, WebRequestEventArgs e) {
            //    e.WebRequestExecutor.WebRequest.CookieContainer = new CookieContainer();
            //    foreach (Cookie cookie in cookies) {
            //        e.WebRequestExecutor.WebRequest.CookieContainer.Add(cookie);
            //    }
            //};
        }

        // need a plain Load method here, the base method is some
        // kind of dynamic method which isn't supported in PowerShell.
        public void Load(ClientObject objectToLoad) {
            base.Load(objectToLoad);
        }
    }
}
