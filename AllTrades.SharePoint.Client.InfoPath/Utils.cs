﻿using System.IO;
using System.Linq;
using System.Xml;
using AllTrades.InfoPath.UDCX;
using Microsoft.SharePoint.Client;

namespace AllTrades.SharePoint.Client.InfoPath {
    public class Utils {
        public static int CreateUDCXFileInLibrary(ClientContext ctx, string webUrl, string libraryTitle, string fileName, UDCXFile dataSource, bool overwrite) {
            FileCreationInformation newFile = new FileCreationInformation();
            if (!fileName.EndsWith(".udcx")) {
                fileName += ".udcx";
            }
            newFile.Url = fileName;
            newFile.Overwrite = overwrite;
            string tempFileName = System.IO.Path.GetTempFileName();
            dataSource.Xml.Save(tempFileName);
            newFile.Content = System.IO.File.ReadAllBytes(tempFileName);

            //using (Stream stream = new MemoryStream()) {
            //    using (var writer = XmlWriter.Create(stream)) {
            //        dataSource.Xml.Save(writer);
            //        writer.Close();
            //        stream.Position = 0;
            //        var buffer = new byte[stream.Length];
            //        stream.Write(buffer, 0, buffer.Length);
            //        stream.Flush();
            //        stream.Close();
            //        newFile.Content = buffer;
            //    }
            //}

            var library = ctx.Web.Lists.GetByTitle(libraryTitle);
            var contentTypes = ctx.LoadQuery(from ct in library.ContentTypes select ct);
            ctx.Load(ctx.Web);
            ctx.Load(library);
            ctx.ExecuteQuery();
            var type = contentTypes.FirstOrDefault(contentType => contentType.Id.ToString().ToLower().StartsWith(UDCXFile.InfopathDataSourceContentTypeId.ToLower()));
            if (type == null) {
                return -1;
            }
            var file = library.RootFolder.Files.Add(newFile);
            library.Update();
            var item = file.ListItemAllFields;
            item["Title"] = Path.GetFileNameWithoutExtension(fileName);
            item["ContentTypeId"] = type.Id.ToString();
            // ReSharper disable PossibleNullReferenceException
            item["ConnectionType"] = dataSource.Xml.Element(UDCXFile.InfoPathDataConnectionNs + "DataSource").Element(UDCXFile.InfoPathDataConnectionNs + "Type").Attribute("Type").Value;
            item["Purpose"] = dataSource.Xml.Element(UDCXFile.InfoPathDataConnectionNs + "DataSource").Element(UDCXFile.InfoPathDataConnectionNs + "ConnectionInfo").Attribute("Purpose").Value;
            item["Comments"] = dataSource.Xml.Element(UDCXFile.InfoPathDataConnectionNs + "DataSource").Element(UDCXFile.InfoPathDataConnectionNs + "Description").Value;
            item.Update();
            ctx.Load(item);
            ctx.ExecuteQuery();
            item["_ModerationStatus"] = 0;
            // ReSharper restore PossibleNullReferenceException
            item.Update();
            ctx.ExecuteQuery();
            return item.Id;
        }
    }
}
