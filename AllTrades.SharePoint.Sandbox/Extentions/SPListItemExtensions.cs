﻿using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Sandbox.Extentions
{
    public static class SPExtensions
    {
        public static bool IsFolder(this SPListItem item)
        {
            return (item.Folder != null);
        }

        public static bool IsDocumentLibrary(this SPList list)
        {
            return (list.BaseType == SPBaseType.DocumentLibrary);
        }
    }
}
