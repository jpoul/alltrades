﻿namespace AllTrades.SharePoint.Sandbox.Structs {
    public struct SPTaskKnownFieldNames {
        public const string BodyFieldName = "Body";
        public const string OutcomeFieldName = "Outcome";
        public const string PercentCompleteFieldName = "PercentComplete";
    }
}