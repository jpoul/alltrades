﻿namespace AllTrades.SharePoint.Sandbox.Structs
{
    public static class CommonCamlPartTemplates
    {
        public const string CamlContentTypeTemplate = "<Eq><FieldRef Name='ContentType' /><Value Type='Computed'>{0}</Value></Eq>";
    }
}
