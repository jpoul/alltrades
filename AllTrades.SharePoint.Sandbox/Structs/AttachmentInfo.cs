﻿using System.IO;

namespace AllTrades.SharePoint.Sandbox.Structs {
    public struct AttachmentInfo {
        public string FileName;
        public Stream Stream;
        public AttachmentInfo(Stream stream, string fileName) {
            Stream = stream;
            FileName = fileName;
        }
    }
}