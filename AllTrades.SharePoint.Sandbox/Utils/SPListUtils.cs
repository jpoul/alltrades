﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AllTrades.SharePoint.Sandbox.Structs;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Sandbox.Utils {
    public static class SPListUtils {
        public static bool IsFolderItem(SPListItem item)
        {
            return item.ContentType.Id.IsChildOf(new SPContentTypeId("0x0120"));
        }
        public static SPContentType GetContentTypeOfListByName(SPList list, string contentTypeName) {
            var contentType = list.ContentTypes[contentTypeName];
            if (!list.AllowContentTypes) {
                throw new InvalidOperationException("This list does not support content types.");
            }
            if (contentType == null) {
                throw new InvalidOperationException(String.Format("Content Type '{0}' not found in list.", contentTypeName));
            }
            return contentType;
        }
        public static void UpdateItemContentType(SPListItem item, SPContentTypeId contentTypeId) {
            item[SPBuiltInFieldId.ContentTypeId] = contentTypeId;
            item.SystemUpdate();
        }
        public static int CreateFolder(SPFolder rootFolder, string folderName, SPContentTypeId contentTypeId) {
            SPFolder newFolder = rootFolder.SubFolders.Add(folderName);
            //newFolder.Item.SystemUpdate();
            //newFolder.SystemUpdate();
            UpdateItemContentType(newFolder.Item, contentTypeId);
            return newFolder.Item.ID;
        }
        public static void HideContentTypes(SPFolder rootFolder, string contentTypeName) {
            var contentTypeCollection = rootFolder.ContentTypeOrder;
            var contentTypes = contentTypeCollection.Where(contentType => !contentType.Name.Equals(contentTypeName)).ToList();
            rootFolder.UniqueContentTypeOrder = contentTypes;
            rootFolder.Update();
        }
        /// <summary>
        ///   gets all of the attachments for a given list item
        /// </summary>
        /// <param name = "listItems"></param>
        /// <returns></returns>
        public static AttachmentInfo[] GetListItemAttachments(SPListItem listItems) {
            var myAttachments = new List<AttachmentInfo>();
            for (int i = 0; i < listItems.Attachments.Count; i++) {
                //attachments are actualy SPFiles stored in SPFolder that is part of our list
                SPFile myAttachmentFile = listItems.Web.GetFile(listItems.Attachments.UrlPrefix + listItems.Attachments[i]);
                var myAi = new AttachmentInfo(myAttachmentFile.OpenBinaryStream(), myAttachmentFile.Name);
                myAttachments.Add(myAi);
            }
            return myAttachments.ToArray();
        }

        /// <summary>
        /// adds a specified role for a given user on a list item
        /// </summary>
        /// <param name="web">ref to SPWeb</param>
        /// <param name="listItem">list item in question</param>
        /// <param name="role">role to add for user e.g. Contribute, View or any custom permission level</param>
        /// <param name="loginName">login name of user or sharepoint group</param>
        /// <returns></returns>
        public static SPListItem SetItemPermissions(SPWeb web, SPListItem listItem, string role, string loginName) {
            if (!listItem.HasUniqueRoleAssignments) {
                listItem.BreakRoleInheritance(true);
            }

            SPRoleDefinition roleDefinition = web.RoleDefinitions[role];

            SPPrincipal myUser = SPSiteUtils.FindUserOrSiteGroup(web.Site, loginName);

            if (myUser == null) {
                throw new ArgumentException(String.Format("user:{0} is not a valid sharepoint user!", loginName));
            }

            var roleAssignment = new SPRoleAssignment(myUser);

            roleAssignment.RoleDefinitionBindings.Add(roleDefinition);

            listItem.RoleAssignments.Add(roleAssignment);

            return listItem;
        }
        /// <summary>
        /// removes any Limited Access Role Assigments from the list item
        /// </summary>
        /// <param name="item"></param>
        public static void RemoveListItemLimitedPermissions(SPListItem item) {
            if (!item.HasUniqueRoleAssignments)
                return;


            var usersToRemove = (from SPRoleAssignment ra in item.RoleAssignments
                                 where ra.RoleDefinitionBindings.Count == 1 && ra.RoleDefinitionBindings.Contains(item.Web.RoleDefinitions.GetByType(SPRoleType.Guest))
                                 select ra.Member.ID).ToList();

            foreach (int id in usersToRemove) {
                item.RoleAssignments.RemoveById(id);
            }
        }
        /// <summary>
        /// removes role assigments for principal, breaking inheritance if specified
        /// </summary>
        /// <param name="item"></param>
        /// <param name="principalName"></param>
        /// <param name="breakRoleInheritance"></param>
        public static void RemoveListItemPermissionEntry(SPListItem item, string principalName,
                                                           bool breakRoleInheritance) {
            if (!item.HasUniqueRoleAssignments && breakRoleInheritance)
                item.BreakRoleInheritance(true);
            else if (!item.HasUniqueRoleAssignments)
                return;

            SPPrincipal myPrincipal = SPSiteUtils.FindUserOrSiteGroup(item.Web.Site, principalName);


            var usersToRemove = (from SPRoleAssignment ra in item.RoleAssignments
                                 where ra.Member.ID == myPrincipal.ID
                                 select ra.Member.ID).ToList();

            foreach (int id in usersToRemove) {
                item.RoleAssignments.RemoveById(id);
            }
        }

        public static void AddDocumentLink(string webUrl, string libraryName, string documentPath, string documentName, string documentUrl)
        {
            using (var site = new SPSite(webUrl))
            {
                using (var web = site.OpenWeb())
                {
                    var docLibrary = web.Lists[libraryName];
                    var contentType = GetContentTypeOfListByName(docLibrary, "Link to a Document");
                    
                    //get full path of the document to add
                    var filePath = docLibrary.RootFolder.ServerRelativeUrl;
                    if (!String.IsNullOrEmpty(documentPath))
                    {
                        filePath += "/" + documentPath;
                    }
                    var currentFolder = web.GetFolder(filePath);

                    var files = currentFolder.Files;
                    var urlOfFile = currentFolder.Url + "/" + documentName + ".aspx";

                    const string format = @"<%@ Assembly Name='{0}' %>
            <%@ Register TagPrefix='SharePoint' Namespace='Microsoft.SharePoint.WebControls' Assembly='Microsoft.SharePoint' %>
            <%@ Import Namespace='System.IO' %>
            <%@ Import Namespace='Microsoft.SharePoint' %>
            <%@ Import Namespace='Microsoft.SharePoint.Utilities' %>
            <%@ Import Namespace='Microsoft.SharePoint.WebControls' %>
                <html>
                    <head> 
                            <meta name='progid' content='SharePoint.Link' /> 
                    </head>
                    <body>
                        <form id='Form1' runat='server'>
                            <SharePoint:UrlRedirector id='Redirector1' runat='server' />
                        </form>
                    </body>
                </html>";

                    var builder = new StringBuilder(format.Length + 400);
                    builder.AppendFormat(format, typeof(SPDocumentLibrary).Assembly.FullName);

                    var properties = new Hashtable();
                    properties["ContentTypeId"] = contentType.Id.ToString();

                    var file = files.Add(urlOfFile, new MemoryStream(new UTF8Encoding().GetBytes(builder.ToString())), properties, false, false);
                    var item = file.Item;
                    item["URL"] = documentUrl + ", ";
                    item.UpdateOverwriteVersion();
                }
            }
        }
        public static SPFolder GetListItemAttachmentFolder(SPListItem listItem) {
            return listItem.ParentList.RootFolder.SubFolders["Attachments"].SubFolders[listItem.ID.ToString()];
        }

        public static void SaveFileToLocalFolder(SPFile file, string localPath) {
            var localFilePath = Path.Combine(localPath, file.Name);
            using (var stream = new FileStream(localFilePath, FileMode.Create)) {
                using (var writer = new BinaryWriter(stream)) {
                    writer.Write(file.OpenBinary());
                    writer.Close();
                }
            }
        }
    }
}