﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace AllTrades.SharePoint.Sandbox.Utils
{
    public  static class SPSiteUtils
    {
        /// <summary>
        /// determines whether a given login name is a sharepoint or active directory and returns a common object is the login name is valid
        /// </summary>
        /// <param name="site"></param>
        /// <param name="userOrGroup"></param>
        /// <returns></returns>
        public static SPPrincipal FindUserOrSiteGroup(SPSite site, string userOrGroup) {
            SPPrincipal myUser = null;

            //is this a user
            if (SPUtility.IsLoginValid(site, userOrGroup)) {
                myUser = site.RootWeb.EnsureUser(userOrGroup);
            }
            else {
                //might be a group
                foreach (SPGroup g in site.RootWeb.SiteGroups) {
                    if (g.Name == userOrGroup)
                        myUser = g;
                }
            }


            return myUser;
        }
        /// <summary>
        /// checks if a user role is assigned - greedy version
        /// </summary>
        /// <param name="site"></param>
        /// <param name="web"></param>
        /// <param name="listId"></param>
        /// <param name="listItem"></param>
        /// <param name="role"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool IsUserRoleAssigned(SPSite site, SPWeb web, string listId, int listItem, string role,
                                                string userName) {
            bool result = false;

            SPList list = web.Lists[new Guid(listId)];

            SPListItem item = list.GetItemById(listItem);

            try {
                SPPrincipal myUser = FindUserOrSiteGroup(site, userName);


                SPRoleAssignment myAssigment = item.RoleAssignments.GetAssignmentByPrincipal(myUser);

                if (myAssigment != null)

                    result = myAssigment.RoleDefinitionBindings.Contains(web.RoleDefinitions[role]);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch
// ReSharper restore EmptyGeneralCatchClause
            {}

            return result;
        }
    }
}
