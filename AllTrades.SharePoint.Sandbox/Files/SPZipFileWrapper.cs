﻿using System;
using System.IO;
using AllTrades.Files;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.SharePoint;

namespace AllTrades.SharePoint.Sandbox.Files {
    public class SPZipFileWrapper : ZipFileWrapper {
        public SPZipFileWrapper(Stream outStream) : base(outStream) { }
        public void AddFile(SPFile file, string folder) {
            using (var fileStream = file.OpenBinaryStream()) {
                Add(folder + "\\" + file.Name, fileStream);
                fileStream.Close();
            }
        }
        public void AddFolder(SPFolder folder, string parentFolder) {
            var folderPath = parentFolder == String.Empty
                                 ? folder.Name
                                 : parentFolder + "\\" + folder.Name;
            AddDirectory(folderPath);
            foreach (SPFile file in folder.Files) {
                AddFile(file, folderPath);
            }
            foreach (SPFolder subFolder in folder.SubFolders) {
                AddFolder(subFolder, folderPath);
            }
        }
        
        public static void ExtractCompressedListItem(SPListItem item) {
            using (var s = new ZipInputStream(item.File.OpenBinaryStream())) {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null) {
                    var data = new byte[s.Length];
                    s.Read(data, 0, data.Length);
                    item.File.ParentFolder.Files.Add(theEntry.Name, data, false);
                }
            }
        }
    }
}