﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllTrades.InfoPath.CommandLineTool {
    class Program {
        static void Main(string[] args) {
            var path = @"C:\Users\pouliej\ownCloud\Projects\WorkflowSteps\FormFiles";
            var filename = @"C:\Users\pouliej\ownCloud\Projects\WorkflowSteps\newForm.xsn";
            AllTrades.InfoPath.Utils.ReplaceInInfoPathPublishedFolder(path, new Hashtable(), filename);
        }
    }
}
