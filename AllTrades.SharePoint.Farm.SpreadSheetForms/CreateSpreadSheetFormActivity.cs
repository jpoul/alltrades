﻿using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions;
using AllTrades.SharePoint.Sandbox.Utils;
using AllTrades.SpreadSheetForms;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using AllTrades.SharePoint.Farm.WorkflowActions.ODC;
using System.Linq;
using System.Net;

namespace AllTrades.SharePoint.Farm.SpreadSheetForms {
    public class CreateSpreadSheetFormActivity : AllTradesActivityBase {
        public static readonly DependencyProperty ItemPropertiesProperty = DependencyProperty.Register("ItemProperties", typeof (Hashtable), typeof (CreateSpreadSheetFormActivity));
        public static readonly DependencyProperty OverwriteProperty = DependencyProperty.Register("Overwrite", typeof (Boolean), typeof (CreateSpreadSheetFormActivity));
        public static DependencyProperty NewItemIdProperty = DependencyProperty.Register("NewItemId", typeof(SPItemKey), typeof(CreateSpreadSheetFormActivity));
        public static readonly DependencyProperty ODCParametersTokenProperty = DependencyProperty.Register("ODCParametersToken", typeof(string), typeof(CreateSpreadSheetFormActivity));
        
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Boolean Overwrite {
            get { return ((Boolean) (GetValue(OverwriteProperty))); }
            set { SetValue(OverwriteProperty, value); }
        }
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible)]
        [BrowsableAttribute(true)]
        [CategoryAttribute("Misc")]
        public Hashtable ItemProperties {
            get { return ((Hashtable) (base.GetValue(ItemPropertiesProperty))); }
            set { base.SetValue(ItemPropertiesProperty, value); }
        }

        public SPItemKey NewItemId {
            get { return (SPItemKey) GetValue(NewItemIdProperty); }
            set {SetValue(NewItemIdProperty, value);}
        }

        public string ODCParametersToken {
            get { return (string) GetValue(ODCParametersTokenProperty); }
            set {SetValue(ODCParametersTokenProperty, value);}
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext) {
            foreach (object key in ItemProperties.Keys) {
                LogToHistory(executionContext, (string) key);
                LogToHistory(executionContext, (string) ItemProperties[key]);
            }

            using (var spSite = new SPSite(__Context.Site.ID)) {
                using (var spWeb = spSite.OpenWeb(__Context.Web.ID)) {
                    SPList list = spWeb.Lists[new Guid(ListId)];
                    var ctId = new SPContentTypeId((string) ItemProperties["ContentTypeId"]);
                    var contentType = list.ContentTypes[ctId];
                    LogToHistory(executionContext, contentType.DocumentTemplateUrl);
                    LogToHistory(executionContext, contentType.DocumentTemplate);
                    SpreadSheetForm form = Utils.CreateSpreadSheetFromContentType(contentType, spWeb);
                    foreach (SpreadSheetFormDataConfiguration c in form.DataConfiguration)
                    {
                        SpreadSheetFormDataConfiguration c1 = c;
                        var parameters = ODCParameter.DeserializeODCParameters(ODCParametersToken).Where(odc => odc.Name == c1.ConnectionName);
                        string[] pars = (parameters.Where(odcParameter => odcParameter.TokenType == ODCTokenType.AddParameter)
                            .Select(odcParameter => odcParameter.ParameterValue)).ToArray();
 
                        foreach (var par in pars) {
                            c.Parameters.Add(par);
                        }

                        foreach (ODCParameter cmdToken in parameters.Where(odcParameter => odcParameter.TokenType == ODCTokenType.ReplaceCommandText))
                        {
                            c.Connection.CommandText = c.Connection.CommandText.Replace(cmdToken.ParameterName, cmdToken.ParameterValue);
                        }
                       
                    }
                    form.Fill();


                    // TODO: create new name if overwrite = false

                    using (Stream memoryStream = new MemoryStream()) {
                        form.PublishTemplate(memoryStream, true, SpreadSheetForm.DefaultPassword);
                        SPFile file = list.RootFolder.Files.Add((string) ItemProperties["FileLeafRef"], memoryStream, ItemProperties, Overwrite);

                        foreach (var key in ItemProperties.Keys) {
                            file.Item[key.ToString()] = ItemProperties[key];
                        }
                        file.Item.SystemUpdate(false);
                    }
                }
            }

            return ActivityExecutionStatus.Closed;
        }
        //private void fillForm_ExecuteCode(object sender, EventArgs e)
        //{
        //    var docLib = (SPDocumentLibrary)__Context.Web.Lists[ListId];
        //    using (var site = new SPSite(__Context.Site.ID))
        //    {
        //        using (var web = site.OpenWeb(__Context.Web.ID))
        //        {
        //    //        var contentType = SPListHelper.GetContentTypeOfListByName(list, ActivityHelper.ProcessStringField(executionContext, ContentTypeName));
        //    //        if (contentType == null)
        //    //        {
        //    //            throw new ArgumentException("Content type not found in list.");
        //    //        }
        //    //        var spreadSheet = Utils.CreateSpreadSheetFromContentType(contentType, site.RootWeb);
        //    //        if (!spreadSheet.IsValidTemplate)
        //    //        {
        //    //            throw new ArgumentException("File is not a spreadsheet template.");
        //    //        }
        //    //        var processedCriteriaTokensString = ActivityHelper.ProcessStringField(executionContext, CriteriaTokens);
        //    //        var processedModelTokensString = ActivityHelper.ProcessStringField(executionContext, ModelTokens);
        //    //        var service = SPFarm.Local.Services.GetValue<XpoService>();
        //    //        var configurations = service.Applications.Cast<XpoServiceApplication>().Select(app => app.ToXpoDataLayerConfiguration()).ToArray();
        //    //        XpoHelper.Preconfigure(configurations);
        //    //        if (XpoHelper.Configurations.Count == 0)
        //    //        {
        //    //            foreach (XpoDataLayerConfiguration configuration in configurations)
        //    //            {
        //    //                XpoHelper.Configurations.Add(configuration.DataLayerIdentifier, configuration);
        //    //            }
        //    //        }
        //    //        spreadSheet.BindDataConfiguration(processedCriteriaTokensString, processedModelTokensString);
        //    //        var stream = spreadSheet.PublishTemplate();
        //    //        var allProps = new Hashtable();
        //    //        if (!string.IsNullOrEmpty(ItemProperties))
        //    //        {
        //    //            foreach (var props in ActivityHelper.ProcessStringField(executionContext, ItemProperties).Split(';'))
        //    //            {
        //    //                if (props.Contains("="))
        //    //                {
        //    //                    allProps.Add(props.Split('=')[0], props.Split('=')[1]);
        //    //                }
        //    //            }
        //    //        }
        //    //        var spFile = folder.Files.Add(ActivityHelper.ProcessStringField(executionContext, FormName), stream, allProps, __Context.InitiatorUser, __Context.InitiatorUser, DateTime.Now, DateTime.Now, "", false);
        //    //        if (allProps.Count > 0)
        //    //        {
        //    //            foreach (DictionaryEntry prop in allProps)
        //    //            {
        //    //                spFile.Item[prop.Key.ToString()] = prop.Value;
        //    //            }
        //    //            spFile.Item.SystemUpdate(false);
        //    //        }
        //    //        FormItemId = spFile.Item.ID;
        //    //    }
        //    //}
        //    //SPListItem spListItem = docLib.GetItemById(NewItemId.Id);
        //    //SPFile file = spListItem.File;
        //    //SpreadSheetForm form = null;
        //    //using (Stream stream = file.OpenBinaryStream())
        //    //{
        //    //    form = new SpreadSheetForm(stream);
        //    //    stream.Close();
        //    //}
        //    //if (!string.IsNullOrEmpty(QueryParameters)) { 
        //    //    var processedQueryParameters = Helper.ProcessStringField(QueryParameters, this, __Context);
        //    //    var parameters = processedQueryParameters.Split(ODCConnectionFile.TokenDelimeter);
        //    //    form.DataConfiguration
        //    //var processedQueryParameters = Helper.ProcessStringField(QueryParameters, this, __Context);
        //    ////var processedModelTokensString = ActivityUtils.ProcessStringField(executionContext, ModelTokens);
        //    //spreadSheet.Fill();
        //    //var stream = spreadSheet.PublishTemplate();
        //            return ActivityExecutionStatus.
        //}
    }
}