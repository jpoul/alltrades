﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Workflow.ComponentModel;
using AllTrades.SharePoint.Farm.WorkflowActions;
using AllTrades.SharePoint.Farm.WorkflowActions.ComponentModel.Design;
using AllTrades.SharePoint.Sandbox.Extentions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.WorkflowActions;

namespace AllTrades.SharePoint.Farm.SpreadSheetForms
{
    [Designer(typeof(AllTradesActivityDesigner), typeof(IDesigner))]
    [ToolboxItem(typeof(AllTradesActivityToolBoxItem))]
    public class CreateSpreadSheetFormActivity123 : AllTradesSequenceActivityBase
    {
        public static readonly DependencyProperty QueryParametersProperty = DependencyProperty.Register("QueryParameters", typeof(string), typeof(CreateSpreadSheetFormActivity123));
        public static readonly DependencyProperty CommandTokensProperty = DependencyProperty.Register("CommandTokens", typeof(string), typeof(CreateSpreadSheetFormActivity123));
        public static readonly DependencyProperty CommandTokenValuesProperty = DependencyProperty.Register("CommandTokenValues", typeof(string), typeof(CreateSpreadSheetFormActivity123));
        public static readonly DependencyProperty OverwriteProperty = DependencyProperty.Register("Overwrite", typeof(Boolean), typeof(CreateSpreadSheetFormActivity123));
        
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Boolean Overwrite
        {
            get { return ((Boolean)(GetValue(OverwriteProperty))); }
            set { SetValue(OverwriteProperty, value); }
        }
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string QueryParameters
        {
            get { return (string)GetValue(QueryParametersProperty); }
            set { SetValue(QueryParametersProperty, value); }
        }
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string CommandTokens
        {
            get { return (string)GetValue(CommandTokensProperty); }
            set { SetValue(CommandTokensProperty, value); }
        }
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string CommandTokenValues
        {
            get { return (string)GetValue(CommandTokenValuesProperty); }
            set { SetValue(CommandTokenValuesProperty, value); }
        }
        protected override ActivityExecutionStatus ExecuteActivityCore(ActivityExecutionContext executionContext)
        {
            using (var site = new SPSite(__Context.Site.ID))
            {
                using (SPWeb web = site.OpenWeb(__Context.Web.ID)) {
                    var list = web.Lists[ListId];
                    if (!(list.IsDocumentLibrary())) {
                        throw new ArgumentException("The list is not a document library");
                    }
                    var docLib = (SPDocumentLibrary) list;
                    
                    //docLib.
                    //var folder = web.GetFolder(formLibUrl);
                    //var list = web.Lists[folder.ParentListId];
                    //var contentType = SPListUtils.GetContentTypeOfListByName(list, ActivityUtils.ProcessStringField(executionContext, ContentTypeName));
                    //if (contentType == null) {
                    //    throw new ArgumentException("Content type not found in list.");
                    //}
                    //var spreadSheet = Utils.CreateSpreadSheetFromContentType(contentType, site.RootWeb);
                    //var processedCriteriaTokensString = ActivityUtils.ProcessStringField(executionContext, CriteriaTokens);
                    //var processedModelTokensString = ActivityUtils.ProcessStringField(executionContext, ModelTokens);

                    //spreadSheet.Fill();
                    //var stream = spreadSheet.PublishTemplate();
                    //var allProps = new Hashtable();
                    //if (!string.IsNullOrEmpty(ItemProperties)) {
                    //    foreach (var props in ActivityUtils.ProcessStringField(executionContext, ItemProperties).Split(';')) {
                    //        if (props.Contains("=")) {
                    //            allProps.Add(props.Split('=')[0], props.Split('=')[1]);
                    //        }
                    //    }
                    //}
                    //var spFile = folder.Files.Add(ActivityUtils.ProcessStringField(executionContext, FormName), stream, allProps, __Context.InitiatorUser, __Context.InitiatorUser, DateTime.Now, DateTime.Now, "", false);
                    //if (allProps.Count > 0) {
                    //    foreach (DictionaryEntry prop in allProps) {
                    //        spFile.Item[prop.Key.ToString()] = prop.Value;
                    //    }
                    //    spFile.Item.SystemUpdate(false);
                    //}
                    //FormItemId = spFile.Item.ID;
                }
            }
            return ActivityExecutionStatus.Closed;
        }
    }
}