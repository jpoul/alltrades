﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="89e0b4c5-998b-49db-93f7-2a5cb0807acc" featureId="89e0b4c5-998b-49db-93f7-2a5cb0807acc" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="SpreadSheet Forms" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="ab9e3608-525d-407e-8ea9-8250e1141e72" projectPath="..\AllTrades.SharePoint.Farm.WorkflowActions\AllTrades.SharePoint.Farm.WorkflowActions.csproj" />
    <referencedFeatureActivationDependency minimumVersion="" itemId="7e778d0b-92da-40f8-9619-47fc12315729" />
    <referencedFeatureActivationDependency minimumVersion="" itemId="b1c7d4be-965e-42ca-83b1-ddf6ef755b7f" projectPath="..\AllTrades.SharePoint.Farm\AllTrades.SharePoint.Farm.csproj" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="23b12d1f-d1c9-4bed-872b-4ade6aa34e09" />
    <projectItemReference itemId="afdb1160-1e77-447b-a6bc-c68b033547d6" />
    <projectItemReference itemId="6a2e3e60-b311-41fe-b6ef-c4b1cc3d09b4" />
  </projectItems>
</feature>