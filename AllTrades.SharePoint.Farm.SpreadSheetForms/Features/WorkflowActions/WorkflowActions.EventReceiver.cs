using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using AllTrades.SharePoint.Farm.Configuration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Security;

namespace AllTrades.SharePoint.Farm.SpreadSheetForms.Features.WorkflowActions
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("207e07a6-50b0-4387-8649-0a631af0a1c3")]
    public class WorkflowActionsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.
        private static IEnumerable<SPWebConfigModification> GetFeatureModifications(SPWebConfigModificationHelper helper)
        {
            var typeOfBase = typeof(CreateSpreadSheetFormActivity);
            return new[] { helper.CreateAuthorizedTypeModification(typeOfBase.Assembly, typeOfBase.Namespace, "*", true), helper.CreateSafeControlModification(typeOfBase.Assembly, typeOfBase.Namespace, typeOfBase.FullName, true) };
        }
        public override void FeatureActivated(SPFeatureReceiverProperties properties) { SPWebConfigModificationHelper.AddFeatureModifications(properties, GetFeatureModifications); }
        // Uncomment the method below to handle the event raised before a feature is deactivated.
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties) { SPWebConfigModificationHelper.RemoveFeatureModifications(properties); }
    }
}
