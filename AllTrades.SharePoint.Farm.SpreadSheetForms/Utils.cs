﻿using System;
using System.Linq;
using System.Net;
using AllTrades.Files;
using AllTrades.SpreadSheetForms;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace AllTrades.SharePoint.Farm.SpreadSheetForms {
    public static class Utils {
        public static SpreadSheetForm CreateSpreadSheetFromContentType(SPContentType contentType, SPWeb web) {
            return CreateSpreadSheetFromContentType(contentType, web, null);
        }
        public static SpreadSheetForm CreateSpreadSheetFromContentType(SPContentType contentType, SPWeb web, NetworkCredential credentials) {
            return CreateSpreadSheetFromContentType(contentType, web, true, credentials);
        }
        public static SpreadSheetForm CreateSpreadSheetFromContentType(SPContentType contentType, SPWeb web, bool useLicense) {
            return CreateSpreadSheetFromContentType(contentType, web, useLicense, null);
        }
        public static SpreadSheetForm CreateSpreadSheetFromContentType(SPContentType contentType, SPWeb web, bool useLicense, NetworkCredential credentials) {
            var spFile = web.GetFile(contentType.DocumentTemplateUrl);
            SpreadSheetForm.UseAsposeLicence = useLicense;
            using (var stream = spFile.OpenBinaryStream()) {
                return new SpreadSheetForm(stream,
                                           delegate(string fileUrl) {
                                               var file = web.GetFile(fileUrl);
                                               using (var conFileStream = file.OpenBinaryStream()) {
                                                   return new ODCConnectionFile(conFileStream);
                                               }
                                           });
            }
        }

        public static Guid CreateFormsLibrary(SPWeb web, string name, string description, string contentTypeName) {
            Guid ret;
            try {
                web.AllowUnsafeUpdates = true;
                var contentType = web.AvailableContentTypes[contentTypeName];
                web.Lists.TryGetList(name);
                var list = web.Lists.TryGetList(name);
                if (list == null) {
                    var docTemplate = web.DocTemplates.Cast<SPDocTemplate>().Where(dt => dt.Type == 100).Single();
                    var listTemplate = web.ListTemplates["SpreadSheet Forms Library"];
                    var guid = web.Lists.Add(name, description, listTemplate, docTemplate);
                    list = web.Lists[guid];
                }
                var listContentType = list.ContentTypes.BestMatch(contentType.Id);
                if (!listContentType.IsChildOf(contentType.Id)) {
                    list.ContentTypes.Add(contentType);
                }
                for (var i = list.ContentTypes.Count; i > 0; i--) {
                    if (!list.ContentTypes[i - 1].Id.IsChildOf(contentType.Id)) {
                        list.ContentTypes[i - 1].Delete();
                    }
                }
                list.Update();
                ret = list.ID;
            } finally {
                web.AllowUnsafeUpdates = false;
            }
            return ret;
        }
    }
}