﻿/*
 * Created by SharpDevelop.
 * User: jpoul
 * Date: 11/07/2012
 * Time: 13:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System.Linq;
using System.Data;
using AllTrades.Files;
using Aspose.Cells;

namespace AllTrades.SpreadSheetForms.Console
{
	class Program
	{
		public static void Main(string[] args)
		{
			//TestODBCTypeConnection();
			//TestOleDbTypeConnection();			
			//TestSampleSpreadSheet();
			//CreateClosingBananceSpreadsheet("Monsoon","1921",DateTime.Now.AddDays(-2));
		}


		public static void TestSampleSpreadSheet() {
			var form = new SpreadSheetForm("TestData\\SpreadSheet.xlsx");
			var c = form.DataConfiguration.Where(cf=>cf.ConnectionName == "ODBC Connection Test").Single();
			c.Parameters.Add("Monsoon");
			c.Parameters.Add("Field");
			form.Fill();
			form.SpreadSheet.Save("TestData\\SpreadSheetFilled.xlsx", SaveFormat.Xlsx);
		}
		
		public static void TestOdbcTypeConnection() {
			RunODCConnectionTest("TestData\\ODBCTest.odc", new object[] {"Monsoon", "Field"});
		}

		public static void TestOleDbTypeConnection() {
			RunODCConnectionTest("TestData\\OLEDBTest.odc", null);
		}

		private static void RunODCConnectionTest(string filePath, object[] parameters) {
			var doc = new ODCConnectionFile(filePath);
			DataSet ds = doc.Execute(parameters);
			foreach (DataRow row in ds.Tables[0].Rows) {
				foreach (DataColumn col in ds.Tables[0].Columns) {
					System.Console.WriteLine(row[col.ColumnName].ToString());
				}
			}
			
			System.Console.WriteLine(@"Press any key to continue . . . ");
			System.Console.ReadKey(true);
		}
	}
}